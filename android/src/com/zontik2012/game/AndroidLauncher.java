package com.zontik2012.game;

import android.os.Bundle;
import android.util.Log;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {
    private MyGame game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("my_tag", "onCreate");
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new MyGame(), config);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("my_tag", "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("my_tag", "onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("my_tag", "onDestroy");
    }
}
