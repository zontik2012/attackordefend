package com.zontik2012.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.zontik2012.game.MyGame;
import com.zontik2012.game.loadingScreen.SomeCoolGame;
import com.zontik2012.game.util.Config;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width= 1280;
		config.height=720;
		new LwjglApplication(new MyGame(), config);
		//new LwjglApplication(new SomeCoolGame(), config);
	}
}
