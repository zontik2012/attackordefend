package com.zontik2012.game.Screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.zontik2012.game.Managers.MusicManager;
import com.zontik2012.game.Managers.ScreenManager;
import com.zontik2012.game.util.Assets;
import com.zontik2012.game.util.Config;
import com.zontik2012.game.util.UIFactory;

/**
 * Main menu class. Nothing more?.
 */
public class MainMenuScreen extends AbstractScreen {

    private Animation<Image> animation;
    private float stateTime;
    private Image logo;
    private Dialog settingsDialog;
    private Assets assets;

    public MainMenuScreen(Assets assets) {
        this.assets = assets;
    }

    @Override
    public int getScreenNum() {
        return AbstractScreen.MAIN_MENU_SCREEN_NUM;
    }

    @Override
    public void buildStage() {

        Texture txtrBg = assets.manager.get(Assets.mainBG);
        Skin uiSkin = assets.manager.get(Assets.uiSkin);
        settingsDialog = new Dialog("", uiSkin);

        // Adding actors
        TextureAtlas logoAtlas = assets.manager.get(Assets.logoAtlas);
        Array<Image> logoFrames = new Array<Image>();
        for (int i = 0; i < logoAtlas.getRegions().size; i++)
            logoFrames.add(new Image(logoAtlas.getRegions().get(i)));
        animation = new Animation<Image>(0.1f, logoFrames);
        animation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);
        logo = new Image(animation.getKeyFrame(0).getDrawable());
        logo.setSize(600, 300);
        logo.setPosition(Config.GAME_WIDTH / 2, 550, Align.center);
        Image bg = new Image(txtrBg);
        bg.setSize(getWidth(), getHeight());

        TextButton.TextButtonStyle btnStyle = new TextButton.TextButtonStyle(uiSkin.optional("bigButton48", TextButton.TextButtonStyle.class));
        TextButton btnPlayTxt = new TextButton("Играть", uiSkin.optional("bigButton48", TextButton.TextButtonStyle.class));
        btnPlayTxt.setPosition(getWidth() / 2, 330, Align.center);
        TextButton btnTutorial = new TextButton("Обучение", uiSkin.optional("bigButton48", TextButton.TextButtonStyle.class));
        btnTutorial.setPosition(getWidth() / 2, btnPlayTxt.getY() - 20, Align.top);

        // Setting listeners
        btnPlayTxt.addListener(UIFactory.showMapListener());
        btnTutorial.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().showTutorial();
//                ScreenManager.getInstance().startTest();
            }
        });
        TextButton btnSettings = new TextButton("Настройки", uiSkin.optional("bigButton48", TextButton.TextButtonStyle.class));
        btnSettings.setPosition(getWidth() / 2, btnTutorial.getY() - 20, Align.top);
        btnSettings.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                settingsDialog.show(MainMenuScreen.this);
            }
        });


        addActor(bg);
        addActor(btnPlayTxt);
        addActor(btnTutorial);
        addActor(btnSettings);
        addActor(logo);

        TextButton.TextButtonStyle settingsBtnStyle = new TextButton.TextButtonStyle(uiSkin.optional("mediumButton42", TextButton.TextButtonStyle.class));
        final TextButton soundBtn = new TextButton("Звук:" + MusicManager.getInstance().getSndString(), settingsBtnStyle);
        soundBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                MusicManager.getInstance().switchMute();
                soundBtn.setText("Звук:" + MusicManager.getInstance().getSndString());
            }
        });
        settingsDialog.getContentTable().add(soundBtn);
        settingsDialog.button(new TextButton("Назад", settingsBtnStyle));
        settingsDialog.getButtonTable().pad(15, 0, 15, 0);
        setReady(true);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        stateTime += delta;
        logo.setDrawable(animation.getKeyFrame(stateTime).getDrawable());
    }
}
