package com.zontik2012.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.zontik2012.game.Managers.MusicManager;
import com.zontik2012.game.Managers.ScreenManager;
import com.zontik2012.game.util.Config;
import com.zontik2012.game.util.Log;

/**
 * Combination of Stage and Screen for best work.
 */
public abstract class AbstractScreen extends Stage implements Screen {

    public static final int TEST_SCREEN_NUM = -1;
    public static final int MAIN_MENU_SCREEN_NUM = 1;
    public static final int MAP_SCREEN_NUM = 2;
    public static final int LEVEL_SCREEN_NUM = 3;
    public static final int TUTORIAL_SCREEN_NUM = 4;
    private boolean ready;

    AbstractScreen() {
        super(new StretchViewport(Config.GAME_WIDTH, Config.GAME_HEIGHT, new OrthographicCamera()));
    }

    /**
     * Setting all the values which wasnt set in constructor
     */
    public abstract void buildStage();

    @Override
    public void render(float delta) {
        // Clear screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Calling to Stage methods
        super.act(delta);
        super.draw();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void resize(int width, int height) {
        getViewport().update(width, height);
    }

    public void fadeOut() {
        Timer.schedule(new Timer.Task() {

            @Override
            public void run() {
                getCamera().translate(0, -Config.GAME_HEIGHT / 100f, 0);

            }
        }, 0, 0.005f, 99);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                ScreenManager.getInstance().getLoadingScreen().unloadPrev();
            }
        }, 1.5f);
    }

    public void fadeIn() {
        getCamera().translate(0, -Config.GAME_HEIGHT, 0);
        Timer.schedule(new Timer.Task() {

            @Override
            public void run() {
                getCamera().translate(0, Config.GAME_HEIGHT / 100f, 0);

            }
        }, 0, 0.005f, 99);
    }

    public int getScreenNum() {
        return 0;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    @Override
    public void pause() {
        Log.logD("pause");
        if (!MusicManager.getInstance().isMuted())
            MusicManager.getInstance().stopOnPause();
    }

    @Override
    public void resume() {
        Log.logD("resume");
        if (!MusicManager.getInstance().isMuted())
            MusicManager.getInstance().playOnResume();
    }

    @Override
    public void hide() {

    }
}
