package com.zontik2012.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.zontik2012.game.Managers.ScreenManager;
import com.zontik2012.game.util.Assets;
import com.zontik2012.game.util.Config;

/**
 * Created by zontik2012 on 02.06.2017.
 */

/**
 * Tutorial Screen. some images and instructions.
 */
public class TutorialScreen extends AbstractScreen {
    private Label word;
    private String[] words;
    private Image stepNow;
    private int step;
    private Assets assets;
    private Skin tutorialSkin;


    public TutorialScreen(Assets assets) {
        this.assets = assets;
    }

    @Override
    public void buildStage() {

        Image bg = new Image(assets.manager.get(Assets.gemsBG));
        bg.setSize(Config.GAME_WIDTH, Config.GAME_HEIGHT);
        Skin uiSkin = assets.manager.get(Assets.uiSkin);
        tutorialSkin = new Skin(assets.manager.get(Assets.tutorialAtlas));


        TextButton.TextButtonStyle textButtonStyle = uiSkin.optional("bigButton48", TextButton.TextButtonStyle.class);
        stepNow = new Image(tutorialSkin, "step1");
        stepNow.setPosition(Config.GAME_WIDTH / 2, Config.GAME_HEIGHT / 2, Align.center);
        words = new String[6];
        words[0] = "1)Выберите клетку, в которой хотите построить башню";
        words[1] = "2)Нажмите нужную кнопку, чтобы построить её";
        words[2] = "3)Перейдите в хранилище камней";
        words[3] = "4)Выберите камень, который хотите вставить в башню";
        words[4] = "4)... И вставьте его на его законное место";
        words[5] = "6)Защищайтесь от волн врагов!";
        word = new Label(words[0], new Label.LabelStyle(new BitmapFont(Gdx.files.internal("UI/fonts/font36.fnt")), Color.WHITE));
        word.setSize(Config.GAME_WIDTH, Config.GAME_HEIGHT - stepNow.getTop() - 20);
        word.setAlignment(Align.center);


        TextButton back = new TextButton("Назад", textButtonStyle);
        TextButton menu = new TextButton("Меню", textButtonStyle);
        TextButton forward = new TextButton("Вперёд", textButtonStyle);
        menu.setPosition(Config.GAME_WIDTH / 2, stepNow.getY() - menu.getHeight() / 2 - 20, Align.center);
        back.setPosition(menu.getX() - 20, menu.getY(), Align.bottomRight);
        forward.setPosition(menu.getRight() + 20, menu.getY(), Align.bottomLeft);
        word.setPosition(0, stepNow.getTop() + 10);

        back.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                step--;
                if (step <= 0)
                    step = 5;
                stepNow.setDrawable(tutorialSkin.getDrawable("step" + (step + 1)));
                word.setText(words[step]);
            }
        });
        menu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().showMenu();
            }
        });
        forward.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                step++;
                if (step >= 6)
                    step = 0;
                stepNow.setDrawable(tutorialSkin.getDrawable("step" + (step + 1)));
                word.setText(words[step]);


            }
        });

        addActor(bg);
        addActor(back);
        addActor(menu);
        addActor(forward);
        addActor(stepNow);
        addActor(word);
        setReady(true);

    }

    @Override
    public void render(float delta) {
        super.render(delta);
    }

    public int getScreenNum() {
        return AbstractScreen.TUTORIAL_SCREEN_NUM;
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
