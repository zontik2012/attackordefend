package com.zontik2012.game.Screens;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;
import com.zontik2012.game.Gems.GemCell;
import com.zontik2012.game.Managers.GemManager;
import com.zontik2012.game.Managers.UIManagerGem;
import com.zontik2012.game.Towers.Tower;
import com.zontik2012.game.util.Assets;
import com.zontik2012.game.util.Config;


public class GemScreen extends AbstractScreen {

    public static final int TOWER_PLACE_X = 200, TOWER_PLACE_Y = Config.GAME_HEIGHT - 200;
    private Tower selectedTower;
    private GemCell towerPlace;
    private Image sellPlace, infoPlace;
    private Skin uiSkin;
    private Assets assets;
    private LevelScreen levelScreen;
    private UIManagerGem uiManager;
    private GemCell[][] gemCells;

    public GemScreen(LevelScreen levelScreen) {
        this.levelScreen = levelScreen;
        assets = levelScreen.getAssets();
        uiSkin = assets.manager.get(Assets.uiSkin);
    }

    @Override
    public void buildStage() {
        Image bg = new Image(assets.manager.get(Assets.gemsBG));
        bg.setSize(Config.GAME_WIDTH, Config.GAME_HEIGHT);
        addActor(bg);

        sellPlace = new Image(uiSkin.getDrawable("buttonMediumDisabled"));
        sellPlace.setSize(150, 150);
        sellPlace.setPosition(860, 710, Align.topRight);
        addActor(sellPlace);

        gemCells = new GemCell[4][7];
        int cellW = 100;
        for (int i = 0, gemNum = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                gemCells[j][i] = new GemCell(new Image(uiSkin.getDrawable("buttonMediumDisabled")), this);
                gemCells[j][i].getImage().setSize(cellW, cellW);
                gemCells[j][i].getImage().setPosition(Config.GAME_WIDTH - cellW * gemCells.length + cellW * j - 10, Config.GAME_HEIGHT - cellW * i - 10, Align.topLeft);
                if (GemManager.getInstance().getGems().size() > gemNum)
                    gemCells[j][i].putGem(GemManager.getInstance().getGems().get(gemNum++));
            }
        }
        towerPlace = new GemCell(new Image(assets.manager.get(Assets.tower)), this);
        towerPlace.getImage().setSize(200, 200);
        towerPlace.getImage().setPosition(TOWER_PLACE_X, TOWER_PLACE_Y, Align.topLeft);

        uiManager = new UIManagerGem(this);
        uiManager.setUpDialogs();
    }

    public void update(Tower selectedTower) {
        this.selectedTower = null;
        if (selectedTower != null) {
            this.selectedTower = selectedTower;
        }
        uiManager.update();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
    }

    public LevelScreen getLevelScreen() {
        return levelScreen;
    }

    public Skin getSkin() {
        return uiSkin;
    }

    public GemCell[][] getGemCells() {
        return gemCells;
    }

    public Tower getDisplayedTower() {
        return selectedTower;
    }

    public Image getSellPlace() {
        return sellPlace;
    }

    public GemCell getTowerPlace() {
        return towerPlace;
    }

    public void setTowerPlace(GemCell towerPlace) {
        this.towerPlace = towerPlace;
    }

    public UIManagerGem getUiManager() {
        return uiManager;
    }


}
