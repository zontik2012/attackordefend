package com.zontik2012.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.zontik2012.game.util.Assets;
import com.zontik2012.game.util.Config;

/**
 * Created by zontik2012 on 12.06.2017.
 */

public class TestScreen extends AbstractScreen {

    Assets assets;
    Skin uiSkin;

    public TestScreen(Assets assets) {
        this.assets = assets;
    }

    @Override
    public int getScreenNum() {
        return AbstractScreen.TEST_SCREEN_NUM;
    }

    @Override
    public void buildStage() {
        uiSkin = assets.manager.get(Assets.uiSkin);
        //create our sprite batch
        Image bg = new Image(assets.manager.get(Assets.gemsBG));
        bg.setSize(Config.GAME_WIDTH, Config.GAME_HEIGHT);
        addActor(bg);
        Image sourceImg = new Image(new Texture(Gdx.files.internal("UI/other/tower_select.png")));
        sourceImg.setBounds(50, 125, 100, 100);
        addActor(sourceImg);

        Image validTargetImage = new Image(new Texture(Gdx.files.internal("UI/other/tower_select.png")));
        validTargetImage.setBounds(200, 50, 100, 100);
        addActor(validTargetImage);

        Image invalidTargetImage = new Image(new Texture(Gdx.files.internal("UI/other/tower_select.png")));
        invalidTargetImage.setBounds(200, 200, 100, 100);
        addActor(invalidTargetImage);

        DragAndDrop dragAndDrop = new DragAndDrop();

        dragAndDrop.addSource(new DragAndDrop.Source(sourceImg) {
            public DragAndDrop.Payload dragStart(InputEvent event, float x, float y, int pointer) {
                DragAndDrop.Payload payload = new DragAndDrop.Payload();
                payload.setObject("Some payload!");

                payload.setDragActor(new Label("Some payload!", uiSkin));

                Label validLabel = new Label("Some payload!", uiSkin);
                validLabel.setColor(0, 1, 0, 1);
                payload.setValidDragActor(validLabel);

                Label invalidLabel = new Label("Some payload!", uiSkin);
                invalidLabel.setColor(1, 0, 0, 1);
                payload.setInvalidDragActor(invalidLabel);

                return payload;
            }
        });
        dragAndDrop.addTarget(new DragAndDrop.Target(validTargetImage) {
            public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                getActor().setColor(Color.GREEN);
                return true;
            }

            public void reset(DragAndDrop.Source source, DragAndDrop.Payload payload) {
                getActor().setColor(Color.WHITE);
            }

            public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                System.out.println("Accepted: " + payload.getObject() + " " + x + ", " + y);
            }
        });
        dragAndDrop.addTarget(new DragAndDrop.Target(invalidTargetImage) {
            public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                getActor().setColor(Color.RED);
                return false;
            }

            public void reset(DragAndDrop.Source source, DragAndDrop.Payload payload) {
                getActor().setColor(Color.WHITE);
            }

            public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
            }
        });

        setReady(true);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
    }

    class GemSource extends DragAndDrop.Source {

        public GemSource(Actor actor) {
            super(actor);
        }

        @Override
        public DragAndDrop.Payload dragStart(InputEvent event, float x, float y, int pointer) {
            return null;
        }
    }
}
