package com.zontik2012.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.zontik2012.game.Enemies.AbstractEnemy;
import com.zontik2012.game.Field.FieldCell;
import com.zontik2012.game.Field.FieldCreator;
import com.zontik2012.game.Managers.GemManager;
import com.zontik2012.game.Managers.TowerManager;
import com.zontik2012.game.Managers.UIManagerLevel;
import com.zontik2012.game.util.Assets;

import java.util.ArrayList;

/**
 * LevelScreen. contains level fields and provides stage/screen for other classes.
 */
public class LevelScreen extends AbstractScreen {

    private Assets assets;
    private FieldCreator fieldCreator;
    private FieldCell[][] fieldCells;
    private int selectedX, selectedY;
    private boolean wave, paused;


    private Timer enemyTimer;
    private ArrayList<AbstractEnemy> enemies;

    private int livesRemainsDefault, livesRemains;
    private long pauseDelay;
    private UIManagerLevel uiManager;
    private TowerManager towerManager;
    private int goldAmountDefault, goldAmount;
    private int gemsAmountDefault;
    private Label goldAmountLabel;
    private Label livesLabel;
    private GemScreen gemScreen;

    private int levelNum;

    private int waveNum = 0, wavesTotal;
    private boolean canFinish;

    private String map;

    public LevelScreen(Integer level, Assets assets) {
        super();
        levelNum = level;
        this.assets = assets;
        goldAmountLabel = new Label("", new Label.LabelStyle(new BitmapFont(Gdx.files.internal("UI/fonts/font36.fnt")), Color.GOLD));
        livesLabel = new Label("", new Label.LabelStyle(new BitmapFont(Gdx.files.internal("UI/fonts/font36.fnt")), Color.FIREBRICK));
        uiManager = new UIManagerLevel(this);
        map = Gdx.files.internal("waves/level_" + level + "/landscape").readString();
        fieldCreator = new FieldCreator(this);
        towerManager = new TowerManager(this);
        enemyTimer = new Timer();
        enemies = new ArrayList<AbstractEnemy>();
        gemScreen = new GemScreen(this);
    }

    @Override
    public int getScreenNum() {
        return AbstractScreen.LEVEL_SCREEN_NUM;
    }

    @Override
    public void buildStage() {
        goldAmountLabel.setAlignment(Align.right);
        livesLabel.setAlignment(Align.right);
        fieldCells = fieldCreator.getFieldCells();
        JsonReader json = new JsonReader();
        JsonValue jsonValue = json.parse(Gdx.files.internal("waves/level_" + levelNum + "/cfg.json"));
        wavesTotal = jsonValue.getInt("waves");
        goldAmountDefault = jsonValue.getInt("gold");
        livesRemainsDefault = jsonValue.getInt("lives");
        gemsAmountDefault = jsonValue.getInt("gems");
        goldAmount = goldAmountDefault;
        livesRemains = livesRemainsDefault;
        fieldCreator.setLvlCells();
        uiManager.addSide();
        setReady(true);
        GemManager.reset(gemsAmountDefault);
        gemScreen.buildStage();
    }

    public void reset() {
        setBuild();
        waveNum = 1;
        while (enemies.size() > 0) {
            enemies.get(0).remove();
            enemies.remove(0);
        }
        goldAmount = goldAmountDefault;
        livesRemains = livesRemainsDefault;
        uiManager.deselectCell();
        towerManager.reset();
        for (int i = 0; i < GemManager.getInstance().getGems().size(); i++)
            GemManager.getInstance().getGems().get(i).getImage().remove();
        GemManager.reset(gemsAmountDefault);

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        goldAmountLabel.setText(String.valueOf(String.valueOf(goldAmount) + " "));
        livesLabel.setText(String.valueOf(livesRemains) + " ");

        if (!paused) {
            fieldCreator.update(delta);
            towerManager.moveBullets(delta);
            towerManager.updateAndShoot(enemies);
        }

        if (wave) {
            if (!paused) {
                for (int i = 0; i < enemies.size(); i++) {
                    enemies.get(i).move(delta);
                    if (enemies.get(i).getCurHP() <= 0) {
                        enemies.get(i).stopFocusingWhenDie();
                        enemies.get(i).getImage().remove();
                        goldAmount += 100;
                        enemies.remove(i);
                        i--;
                        tryFinish();
                        continue;
                    }
                    if (enemies.get(i).isReachedEnd()) {
                        livesRemains--;
                        enemies.get(i).stopFocusingWhenDie();
                        enemies.get(i).getImage().remove();
                        enemies.remove(i);
                        i--;
                        tryFinish();
                    }
                }
            }
        } else {
            towerManager.moveBullets(delta);
        }


    }

    private void setBuild() {
        enemyTimer.clear();
        uiManager.setMenuBuild();
        paused = false;
        canFinish = false;
        wave = false;
    }

    public TowerManager getTowerMgr() {
        return towerManager;
    }

    public FieldCell[][] getField() {
        return fieldCells;
    }

    public void setCurrentCell(int x, int y) {
        selectedX = x;
        selectedY = y;
        uiManager.select(x, y);
    }

    public int getCurTwrX() {
        return selectedX;
    }

    public int getCurTwrY() {
        return selectedY;
    }

    public boolean isWave() {
        return wave;
    }

    public void setWave(boolean wave) {
        this.wave = wave;
    }

    public Timer getEnemyTimer() {
        return enemyTimer;
    }

    public ArrayList<AbstractEnemy> getEnemies() {
        return enemies;
    }

    public FieldCreator getFieldCreator() {
        return fieldCreator;
    }

    public UIManagerLevel getUiManager() {
        return uiManager;
    }

    public int getGold() {
        return goldAmount;
    }

    public void addGold(int add) {
        goldAmount += add;
    }

    public void takeGold(int take) {
        goldAmount -= take;
    }

    public int getLevelNum() {
        return levelNum;
    }

    public int getWaveNum() {
        return waveNum;
    }

    public void addWaveNum() {
        waveNum++;
    }

    public Label getGoldAmountLabel() {
        return goldAmountLabel;
    }

    public Label getLivesLabel() {
        return livesLabel;
    }

    private void tryFinish() {
        if (canFinish) {
            if (livesRemains <= 0) {
                uiManager.lose();
            } else if (enemies.size() <= 0 && livesRemains > 0) {
                if (waveNum >= wavesTotal) uiManager.win();
                else
                    setBuild();
            }
        }
    }

    public void allowFinish() {
        canFinish = true;
    }

    public void pauseLevel() {
        pauseDelay = TimeUtils.nanosToMillis(TimeUtils.nanoTime());
        towerManager.getShotCdTimer().stop();
        enemyTimer.stop();
        paused = true;
    }

    public void unpauseLevel() {
        enemyTimer.delay(TimeUtils.nanosToMillis(TimeUtils.nanoTime()) - pauseDelay);
        towerManager.getShotCdTimer().delay(TimeUtils.nanosToMillis(TimeUtils.nanoTime()) - pauseDelay);
        towerManager.getShotCdTimer().start();
        enemyTimer.start();
        paused = false;
    }

    public Assets getAssets() {
        return assets;
    }

    public String getMap() {
        return map;
    }

    public GemScreen getGemScreen() {
        return gemScreen;
    }

    public void setGemScreen(GemScreen gemScreen) {
        this.gemScreen = gemScreen;
    }

    @Override
    public void dispose() {
        gemScreen.dispose();
    }

    @Override
    public boolean keyDown(int keyCode) {
        if (keyCode == Input.Keys.BACK) {
            UIManagerLevel.CURRENT_DIALOG = UIManagerLevel.DIALOG_QUIT;
            uiManager.showDealDialog(0);
            return false;
        } else
            return super.keyDown(keyCode);
    }

    @Override
    public void pause() {
        super.pause();
        pauseLevel();
    }

    @Override
    public void resume() {
        super.resume();
        unpauseLevel();
    }
}
