package com.zontik2012.game.Screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.zontik2012.game.Managers.MusicManager;
import com.zontik2012.game.Managers.ScreenManager;
import com.zontik2012.game.util.Assets;
import com.zontik2012.game.util.Config;

public class LoadingScreen extends AbstractScreen {

    private Assets assets;
    private Texture blackBg;
    private AbstractScreen prevScreen;
    private AbstractScreen nextScreen;
    private boolean first;

    public LoadingScreen(Assets assets, AbstractScreen prevScreen, AbstractScreen nextScreen) {
        this.assets = assets;
        this.prevScreen = prevScreen;
        this.nextScreen = nextScreen;
    }

    public LoadingScreen(Assets assets) {
        this.assets = assets;
    }

    public void startLoading(AbstractScreen nextScreen) {
        this.nextScreen=nextScreen;
        loadNext();
    }

    public void startLoading(AbstractScreen nextScreen, AbstractScreen prevScreen) {
        prevScreen.fadeOut();
        this.prevScreen=prevScreen;
        this.nextScreen=nextScreen;
    }

    public void unloadPrev() {
        prevScreen.dispose();
        switch (prevScreen.getScreenNum()) {
            case AbstractScreen.LEVEL_SCREEN_NUM: {
                assets.unloadLevel();
            }
            case AbstractScreen.MAP_SCREEN_NUM: {
                assets.unloadMap();
            }
            case AbstractScreen.MAIN_MENU_SCREEN_NUM: {
                assets.unloadMenu();
            }
            case AbstractScreen.TUTORIAL_SCREEN_NUM: {
                assets.unloadTutorial();
            }
        }
        while (!assets.manager.update());
        loadNext();
    }

    public void loadNext() {
        ScreenManager.getInstance().setScreen(LoadingScreen.this);
        switch (nextScreen.getScreenNum()) {
            case AbstractScreen.TEST_SCREEN_NUM: {
                assets.loadTest();
            }
            case AbstractScreen.LEVEL_SCREEN_NUM: {
                assets.loadLevel();
            }
            case AbstractScreen.MAP_SCREEN_NUM: {
                assets.loadMap();
            }
            case AbstractScreen.MAIN_MENU_SCREEN_NUM: {
                assets.loadMenu();
            }
            case AbstractScreen.TUTORIAL_SCREEN_NUM: {
                assets.loadTutorial();
            }
        }
        while (!assets.manager.update())
            /*waiting assetManager to load assets*/ ;
        nextScreen.buildStage();
        switch (nextScreen.getScreenNum()) {
            case AbstractScreen.LEVEL_SCREEN_NUM: {
                GemScreen gemScreen = new GemScreen((LevelScreen) nextScreen);
                ScreenManager.getInstance().setLevelScreen((LevelScreen) nextScreen);
                MusicManager.getInstance().switchTo(Assets.levelMusic);
                gemScreen.buildStage();
            }
            case AbstractScreen.MAP_SCREEN_NUM: {
                if (prevScreen.getScreenNum() == AbstractScreen.LEVEL_SCREEN_NUM) {
                    MusicManager.getInstance().switchTo(Assets.menuMusic);
                }
            }
            case AbstractScreen.MAIN_MENU_SCREEN_NUM: {
                if (prevScreen == null) MusicManager.getInstance().startMusicOnLaunch(assets);
            }
            case AbstractScreen.TUTORIAL_SCREEN_NUM: {

            }
        }

    }

    @Override
    public void buildStage() {
        blackBg = new Texture("UI/black.png");
        Image blackBG = new Image(blackBg);
        blackBG.setSize(Config.GAME_WIDTH, Config.GAME_HEIGHT);
        addActor(blackBG);
        first = true;
    }

    @Override
    public void render(float delta) {

        if (nextScreen.isReady() && assets.manager.update()) {
            ScreenManager.getInstance().setScreen(nextScreen);
            nextScreen.fadeIn();
        }
    }

    @Override
    public void dispose() {
        blackBg.dispose();
    }

    public void setPrevScreen(AbstractScreen prevScreen) {
        this.prevScreen = prevScreen;
    }

    public void setNextScreen(AbstractScreen nextScreen) {
        this.nextScreen = nextScreen;
    }
}
