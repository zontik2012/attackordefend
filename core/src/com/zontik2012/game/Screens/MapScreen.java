package com.zontik2012.game.Screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.zontik2012.game.util.Assets;
import com.zontik2012.game.util.Config;
import com.zontik2012.game.util.UIFactory;

import java.util.ArrayList;


public class MapScreen extends AbstractScreen {

    private Assets assets;

    public MapScreen(Assets assets) {
        this.assets = assets;
    }

    @Override
    public int getScreenNum() {
        return AbstractScreen.MAP_SCREEN_NUM;
    }

    @Override
    public void buildStage() {

        Texture txtrBg = assets.manager.get(Assets.map);
        Skin uiSkin = assets.manager.get(Assets.uiSkin);
        Image bg = new Image(txtrBg);
        bg.setSize(getWidth(), getHeight());
        addActor(bg);

        TextButton btnBack = new TextButton("Назад", uiSkin.optional("mediumButton36", TextButton.TextButtonStyle.class));
        btnBack.setPosition(50, 50);
        addActor(btnBack);

        ArrayList<TextButton> levelBtns = new ArrayList<TextButton>();
        for (int i = 0; i < 3; i++) {
            TextButton btnLevel = new TextButton(String.valueOf(i + 1), uiSkin.optional("levelBtn", TextButton.TextButtonStyle.class));
            btnLevel.setPosition(Config.LVL_BTN_X[i], Config.LVL_BTN_Y[i], Align.center);
            addActor(btnLevel);
            levelBtns.add(btnLevel);
        }
        for (int i = 0; i < levelBtns.size(); i++) {
            levelBtns.get(i).addListener(UIFactory.startLevelListener(i + 1));
        }

        btnBack.addListener(UIFactory.toMainMenuListener());
        setReady(true);
    }
}
