package com.zontik2012.game.Gems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.zontik2012.game.Buffs.Buff;
import com.zontik2012.game.Buffs.DamageOverTime;
import com.zontik2012.game.Buffs.DecreaseDefense;
import com.zontik2012.game.Buffs.MaximumHealthDecrease;
import com.zontik2012.game.Buffs.Slow;
import com.zontik2012.game.Buffs.Stun;
import com.zontik2012.game.util.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

/**
 * Gem class. Contains data to setup towers and some methods to work with it.
 */
public class Gem {
    public static final int ELEMENT_FIRE = 1;
    public static final int ELEMENT_WATER = 2;
    public static final int ELEMENT_LIGHTNING = 3;
    public static final int ELEMENT_EARTH = 4;
    public static final int ELEMENT_AIR = 5;
    public static final int ELEMENT_POISON = 6;
    public static final int ELEMENT_ICE = 7;
    public static final int ELEMENT_LAVA = 8;
    public static final int ELEMENT_DEATH = 9;
    private static final int ATTACK_TYPE_BULLET = 1;
    private static final int ATTACK_TYPE_INSTANT = 2;
    private static final int ATTACK_TYPE_CONSTANT = 3;
    private int damage;
    private float coolDown;
    private int attackType;//1-снаряд,2-мгновенный урон(молния),3-постоянный
    private int targetsCount;
    private float range;
    private Color gemColor;
    private HashSet<Element> elements = new HashSet<Element>();
    private int element;
    private ArrayList<Buff> appliedBuffs;
    private int level = 1, rank = 1;
    private Image image;

    /**
     * Creates new gem.
     *
     * @param damage       Damage it deals with a single attack
     * @param coolDown     Interval between attacks
     * @param targetsCount How many targets gem attack at once
     * @param range        How far gem attack. 1 equals to 1 cell
     * @param element      Sets behavior of the gem
     */
    public Gem(int damage, float coolDown, int attackType, int targetsCount, int range, int element) {
        this.damage = damage;
        this.coolDown = coolDown;
        this.targetsCount = targetsCount;
        this.range = range * Config.CELL_W + Config.CELL_W / 2;
        this.elements.add(new Element(element));
        this.element = element;
        setImage(Gdx.files.internal("Gems/" + String.valueOf(level + 2) + "sideGem.png"));
        getImage().setColor(gemColor);
    }

    public Gem(int damage, float coolDown, int attackType, int targetsCount, int range, int element, int level) {
        this.damage = damage;
        this.coolDown = coolDown;
        this.attackType = attackType;
        this.targetsCount = targetsCount;
        this.range = range;
        this.elements.add(new Element(element));
        this.element = element;
        this.level = level;
    }

    private void setImage(FileHandle fileHandle) {
        Texture texture = new Texture(fileHandle);
        image = new Image(texture);
        image.setSize(Config.CELL_W, Config.CELL_H);
        image.setTouchable(Touchable.disabled);
    }

    private void elementSetting() {
        element_N_C[] element_n_cs = new element_N_C[9];
        for (Iterator<Element> i = elements.iterator(); i.hasNext(); ) {
            Element element = i.next();
            element_n_cs[element.getElement()].howMany++;
            switch (element.getElement()) {
                case ELEMENT_FIRE:
                    gemColor = Color.SCARLET;
                    this.attackType = ATTACK_TYPE_BULLET;
                    appliedBuffs.add(new DamageOverTime(30, 2, this));
                    break;
                case ELEMENT_WATER:
                    gemColor = Color.ROYAL;
                    this.attackType = ATTACK_TYPE_BULLET;
                    appliedBuffs.add(new Slow(10, 1, this));
                    break;
                case ELEMENT_LIGHTNING:
                    gemColor = Color.YELLOW;
                    this.attackType = ATTACK_TYPE_INSTANT;
                    appliedBuffs.add(new Stun(0.1f, this));
                    appliedBuffs.add(new MaximumHealthDecrease(5, 0, this));
                    break;
                case ELEMENT_EARTH:
                    gemColor = Color.BROWN;
                    this.attackType = ATTACK_TYPE_BULLET;
                    appliedBuffs.add(new Stun(1, this));
                    break;
                case ELEMENT_AIR:
                    gemColor = Color.SKY;
                    this.attackType = ATTACK_TYPE_BULLET;
                    appliedBuffs.add(new DecreaseDefense(10, 1.5f, this));
                    break;
                case ELEMENT_POISON:
                    gemColor = Color.OLIVE;
                    this.attackType = ATTACK_TYPE_BULLET;
                    appliedBuffs.add(new DamageOverTime(10, 3, this));
                    appliedBuffs.add(new Slow(20, 3, this));
                    break;
                case ELEMENT_ICE:
                    gemColor = Color.CYAN;
                    this.attackType = ATTACK_TYPE_BULLET;
                    appliedBuffs.add(new Stun(0.7f, this));
                    break;
                case ELEMENT_LAVA:
                    gemColor = Color.FIREBRICK;
                    this.attackType = ATTACK_TYPE_BULLET;
                    appliedBuffs.add(new DamageOverTime(45, 3, this));
                    appliedBuffs.add(new Slow(10, 3, this));
                    break;
                case ELEMENT_DEATH:
                    gemColor = Color.BLACK;
                    this.attackType = ATTACK_TYPE_CONSTANT;
                    appliedBuffs.add(new Slow(10, 0, this));
                    appliedBuffs.add(new MaximumHealthDecrease(10, 5, this));
                    break;
            }
        }

        Arrays.sort(element_n_cs, new Comparator<element_N_C>() {
            @Override
            public int compare(element_N_C element_n_c, element_N_C t1) {
                if (element_n_c.howMany < t1.howMany) return -1;
                else if (element_n_c.howMany == t1.howMany) return 0;
                else return 1;
            }
        });
        int amount = 0;
        for (int i = 0; i < element_n_cs.length - 1; i++) {
            if (element_n_cs[i].howMany == element_n_cs[i + 1].howMany) amount++;
            else break;
        }
        element = element_n_cs[new Random().nextInt(amount) + 1].elementNum;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public float getCoolDown() {
        return coolDown;
    }

    public void setCoolDown(float coolDown) {
        this.coolDown = coolDown;
    }

    public int getAttackType() {
        return attackType;
    }

    public void setAttackType(int attackType) {
        this.attackType = attackType;
    }

    public int getTargetsCount() {
        return targetsCount;
    }

    public void setTargetsCount(int targetsCount) {
        this.targetsCount = targetsCount;
    }

    public float getRange() {
        return range;
    }

    public void setRange(float range) {
        this.range = range;
    }

    public ArrayList<Element> getElements() {
        return elements;
    }

    public void setElements(ArrayList<Element> elements) {
        this.elements = elements;
    }

    public int getElement() {
        return element;
    }

    public int getLevel() {
        return level;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public Color getGemColor() {
        return gemColor;
    }
}

class element_N_C {
    int howMany;
    int elementNum;
}