package com.zontik2012.game.Gems;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.zontik2012.game.Screens.GemScreen;
import com.zontik2012.game.util.Config;

/**
 * Created by zontik2012 on 03.09.2017.
 */

public class GemCell {

    private Gem hasGem;
    private GemScreen gemScreen;
    private Image image;

    public GemCell(Image image, Gem gem, GemScreen gemScreen) {
        this.image = image;
        this.gemScreen = gemScreen;
    }

    public GemCell(Image image, GemScreen gemScreen) {
        this.image = image;
        this.gemScreen = gemScreen;
    }

    public boolean hasGem() {
        return (hasGem != null);
    }

    public void putGem(Gem gem) {
        hasGem = gem;
        gem.getImage().setZIndex(image.getZIndex() + 1);
        gem.getImage().setSize(Config.GEM_SCREEN_GEM_SIZE, Config.GEM_SCREEN_GEM_SIZE);
        gem.getImage().setPosition(image.getX() + image.getWidth() / 2, image.getY() + image.getHeight() / 2, Align.center);
        gemScreen.addActor(gem.getImage());
    }

    public Gem getGem() {
        return hasGem;
    }

    public Gem takeGem() {
        Gem result = hasGem;
        hasGem.getImage().remove();
        hasGem = null;
        return result;
    }

    public Image getImage() {
        return image;
    }
}
