package com.zontik2012.game.Gems;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class Element {
    private float percentage;
    private int element;

    Element(int element) {
        this.element = element;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    public int getElement() {
        return element;
    }

    public void setElement(int element) {
        this.element = element;
    }
}
