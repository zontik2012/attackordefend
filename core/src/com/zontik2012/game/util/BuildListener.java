package com.zontik2012.game.util;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.zontik2012.game.Managers.UIManagerLevel;
import com.zontik2012.game.Screens.LevelScreen;


/**
 * Not really build, but still - listener for LevelScreen Image.
 */
public class BuildListener extends InputListener {
    private int X, Y;
    private LevelScreen lvlScreen;
    private UIManagerLevel uiManager;

    public BuildListener(LevelScreen levelScreen, int x, int y) {
        X = x;
        Y = y;
        lvlScreen = levelScreen;
        this.uiManager = levelScreen.getUiManager();
    }

    @Override
    public boolean touchDown(InputEvent event, float x,
                             float y, int pointer, int button) {
        if (uiManager.isSelect()) {
            uiManager.deselectCell();
//                X = Math.max(0, Math.min((int) x / Config.CELL_W, 14));
//                Y = Math.max(0, Math.min((int) y / Config.CELL_H, 9));
            if (!(lvlScreen.getCurTwrX() == X && lvlScreen.getCurTwrY() == Y))
                lvlScreen.setCurrentCell(X, Y);
        } else {
            lvlScreen.setCurrentCell(X, Y);
        }

        return false;
    }
}
