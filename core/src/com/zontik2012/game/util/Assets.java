package com.zontik2012.game.util;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created by zontik2012 on 11.06.2017.
 */

public class Assets {

    //------MUSIC------\\
    public static final AssetDescriptor<Music> menuMusic =
            new AssetDescriptor<Music>("sound/music/menu.mp3", Music.class);
    public static final AssetDescriptor<Music> levelMusic =
            new AssetDescriptor<Music>("sound/music/level.mp3", Music.class);


    //------UI------\\
    public static final AssetDescriptor<Skin> uiSkin =
            new AssetDescriptor<Skin>("UI/uiSkin.skin", Skin.class);

    public static final AssetDescriptor<Texture> gemsBG =
            new AssetDescriptor<Texture>("bgs/BG_gems.png", Texture.class);
    public static final AssetDescriptor<Texture> mainBG =
            new AssetDescriptor<Texture>("bgs/main_menu_bg.png", Texture.class);
    public static final AssetDescriptor<Texture> map =
            new AssetDescriptor<Texture>("bgs/map.png", Texture.class);

    public static final AssetDescriptor<TextureAtlas> logoAtlas =
            new AssetDescriptor<TextureAtlas>("UI/Logo.atlas", TextureAtlas.class);

    //------TYPE_GROUND------\\
    public static final AssetDescriptor<Texture> cobbleRoad =
            new AssetDescriptor<Texture>("tiles/road/dirtRoad.png", Texture.class);
    public static final AssetDescriptor<TextureAtlas> road =
            new AssetDescriptor<TextureAtlas>("tiles/road/road.atlas", TextureAtlas.class);
    public static final AssetDescriptor<Skin> roadSkin =
            new AssetDescriptor<Skin>("tiles/road/road.skin", Skin.class);
    public static final AssetDescriptor<Texture> portal =
            new AssetDescriptor<Texture>("tiles/Portal.png", Texture.class);
    public static final AssetDescriptor<Texture> groundForestDay =
            new AssetDescriptor<Texture>("tiles/bg_day.png", Texture.class);
    public static final AssetDescriptor<TextureAtlas> water =
            new AssetDescriptor<TextureAtlas>("tiles/water/water.atlas", TextureAtlas.class);
    public static final AssetDescriptor<TextureAtlas> lightGrassAtlas =
            new AssetDescriptor<TextureAtlas>("tiles/grass/lightGrass.atlas", TextureAtlas.class);
    public static final AssetDescriptor<TextureAtlas> lightGrassCoastAtlas =
            new AssetDescriptor<TextureAtlas>("tiles/water/coast.atlas", TextureAtlas.class);

    //------Enemies------\\
    public static final AssetDescriptor<TextureAtlas> spiderAtlas =
            new AssetDescriptor<TextureAtlas>("Enemies/Spider.atlas", TextureAtlas.class);
    public static final AssetDescriptor<TextureAtlas> skorpionAtlas =
            new AssetDescriptor<TextureAtlas>("Enemies/Scorpion.atlas", TextureAtlas.class);
    public static final AssetDescriptor<TextureAtlas> phoenixAtlas =
            new AssetDescriptor<TextureAtlas>("Enemies/Phoenix.atlas", TextureAtlas.class);

    //------Bullets------\\
    public static final AssetDescriptor<Texture> smartBullet =
            new AssetDescriptor<Texture>("bullets/smart.png", Texture.class);
    public static final AssetDescriptor<Texture> lightning =
            new AssetDescriptor<Texture>("bullets/lightning.png", Texture.class);

    //------Tutorial------\\
    public static final AssetDescriptor<Skin> tutorialSkin =
            new AssetDescriptor<Skin>("tutorial/tutorial.skin", Skin.class);
    public static final AssetDescriptor<TextureAtlas> tutorialAtlas =
            new AssetDescriptor<TextureAtlas>("tutorial/tutorial.atlas", TextureAtlas.class);
    //------Tower------\\
    public static final AssetDescriptor<Texture> tower =
            new AssetDescriptor<Texture>("Towers/tower.png", Texture.class);
    public AssetManager manager;


    public Assets() {
        manager = new AssetManager();
    }

    public void loadGame() {
        manager.load(uiSkin);
        manager.load(menuMusic);
    }

    public void loadMenu() {
        manager.load(mainBG);
        manager.load(logoAtlas);

    }

    public void unloadMenu() {
        manager.unload(mainBG.fileName);
        manager.unload(logoAtlas.fileName);

    }

    public void loadTutorial() {
        manager.load(gemsBG);
        manager.load(tutorialAtlas);

    }

    public void unloadTutorial() {
        manager.unload(gemsBG.fileName);
        manager.unload(tutorialAtlas.fileName);

    }

    public void loadMap() {
        manager.load(map);

    }

    public void unloadMap() {
        manager.unload(map.fileName);

    }

    public void loadLevel() {
        manager.load(levelMusic);
        manager.load(portal);
        manager.load(groundForestDay);
        manager.load(skorpionAtlas);
        manager.load(spiderAtlas);
        manager.load(phoenixAtlas);
        manager.load(water);
        manager.load(lightGrassAtlas);
        manager.load(lightGrassCoastAtlas);
        manager.load(cobbleRoad);
        manager.load(road);
        manager.load(roadSkin);
        manager.load(smartBullet);
        manager.load(lightning);
        manager.load(gemsBG);
        manager.load(tower);
    }

    public void unloadLevel() {
        manager.unload(levelMusic.fileName);
        manager.unload(cobbleRoad.fileName);
        manager.unload(portal.fileName);
        manager.unload(groundForestDay.fileName);
        manager.unload(skorpionAtlas.fileName);
        manager.unload(spiderAtlas.fileName);
        manager.unload(phoenixAtlas.fileName);
        manager.unload(water.fileName);
        manager.unload(lightGrassAtlas.fileName);
        manager.unload(lightGrassCoastAtlas.fileName);
        manager.unload(road.fileName);
        manager.unload(smartBullet.fileName);
        manager.unload(lightning.fileName);
        manager.unload(gemsBG.fileName);
        manager.unload(tower.fileName);
    }

    public void loadTest() {
        manager.load(mainBG);
        manager.load(logoAtlas);
        manager.load(tutorialAtlas);
        manager.load(map);
        manager.load(levelMusic);
        manager.load(portal);
        manager.load(groundForestDay);
        manager.load(skorpionAtlas);
        manager.load(spiderAtlas);
        manager.load(phoenixAtlas);
        manager.load(water);
        manager.load(lightGrassAtlas);
        manager.load(lightGrassCoastAtlas);
        manager.load(cobbleRoad);
        manager.load(road);
        manager.load(smartBullet);
        manager.load(lightning);
        manager.load(gemsBG);
        manager.load(uiSkin);
        manager.load(menuMusic);

    }


    public void dispose() {
        manager.dispose();
    }
}