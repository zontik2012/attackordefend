package com.zontik2012.game.util;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.zontik2012.game.Managers.ScreenManager;

public class UIFactory {


    public static ClickListener showMapListener() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {

                Log.logD("click");
                ScreenManager.getInstance().showMap();
            }
        };
    }

    public static ClickListener toMainMenuListener() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {

                Log.logD("click");
                ScreenManager.getInstance().showMenu();
            }
        };
    }

    public static ClickListener startLevelListener(final int levelNum) {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {

                Log.logD("click");
                ScreenManager.getInstance().startLevel(levelNum);
            }
        };
    }
}
