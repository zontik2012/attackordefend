package com.zontik2012.game.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Created by zontik2012 on 11.08.2017.
 */

public class Log {
    public static void logD(String log) {
        switch (Gdx.app.getType()) {
            case Android:
                Gdx.app.log("my_tag", log);
                break;
            case Desktop:
                Gdx.app.log("my_tag", TimeUtils.nanosToMillis(TimeUtils.nanoTime()) + ": " + log);
        }
    }

    public static void logD(boolean log) {
        switch (Gdx.app.getType()) {
            case Android:
                Gdx.app.log("my_tag", String.valueOf(log));
                break;
            case Desktop:
                Gdx.app.log("my_tag", TimeUtils.nanosToMillis(TimeUtils.nanoTime()) + ": " + log);
        }
    }

    public static void logD(byte log) {
        switch (Gdx.app.getType()) {
            case Android:
                Gdx.app.log("my_tag", String.valueOf(log));
                break;
            case Desktop:
                Gdx.app.log("my_tag", TimeUtils.nanosToMillis(TimeUtils.nanoTime()) + ": " + log);
        }
    }

    public static void logD(char log) {
        switch (Gdx.app.getType()) {
            case Android:
                Gdx.app.log("my_tag", String.valueOf(log));
                break;
            case Desktop:
                Gdx.app.log("my_tag", TimeUtils.nanosToMillis(TimeUtils.nanoTime()) + ": " + log);
        }
    }

    public static void logD(double log) {
        switch (Gdx.app.getType()) {
            case Android:
                Gdx.app.log("my_tag", String.valueOf(log));
                break;
            case Desktop:
                Gdx.app.log("my_tag", TimeUtils.nanosToMillis(TimeUtils.nanoTime()) + ": " + log);
        }
    }

    public static void logD(float log) {
        switch (Gdx.app.getType()) {
            case Android:
                Gdx.app.log("my_tag", String.valueOf(log));
                break;
            case Desktop:
                Gdx.app.log("my_tag", TimeUtils.nanosToMillis(TimeUtils.nanoTime()) + ": " + log);
        }
    }

    public static void logD(int log) {
        switch (Gdx.app.getType()) {
            case Android:
                Gdx.app.log("my_tag", String.valueOf(log));
                break;
            case Desktop:
                Gdx.app.log("my_tag", TimeUtils.nanosToMillis(TimeUtils.nanoTime()) + ": " + log);
        }
    }

    public static void logD(long log) {
        switch (Gdx.app.getType()) {
            case Android:
                Gdx.app.log("my_tag", String.valueOf(log));
                break;
            case Desktop:
                Gdx.app.log("my_tag", TimeUtils.nanosToMillis(TimeUtils.nanoTime()) + ": " + log);
        }
    }

    public static void logD(short log) {
        switch (Gdx.app.getType()) {
            case Android:
                Gdx.app.log("my_tag", String.valueOf(log));
                break;
            case Desktop:
                Gdx.app.log("my_tag", TimeUtils.nanosToMillis(TimeUtils.nanoTime()) + ": " + log);
        }
    }

}
