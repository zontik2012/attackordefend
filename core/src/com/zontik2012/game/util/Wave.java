package com.zontik2012.game.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.zontik2012.game.Enemies.AbstractEnemy;
import com.zontik2012.game.Screens.LevelScreen;

import java.util.ArrayList;

public class Wave {
    private boolean last;
    private ArrayList<AbstractEnemy> enemies;
    private float period;//1.0f=1sec


    public Wave() {
    }

    public ArrayList<AbstractEnemy> getEnemies() {
        return enemies;
    }

    public float getPeriod() {
        return period;
    }

    public boolean isLast() {
        return last;
    }

    public void setWave(LevelScreen levelScreen) {
        enemies = new ArrayList<AbstractEnemy>();
        int levelNum = levelScreen.getLevelNum();
        int waveNum = levelScreen.getWaveNum();

        JsonReader json = new JsonReader();
        JsonValue jsonValue = json.parse(Gdx.files.internal("waves/level_" + levelNum + "/wave_" + waveNum + ".json"));
        Class<?> c = null;

        try {
            c = Class.forName(jsonValue.getString("enemy"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < jsonValue.getInt("enemiesAmount", 0); i++)
            try {
                AbstractEnemy enemy = c != null ? (AbstractEnemy) c.newInstance() : null;
                enemies.add(enemy);
                if (enemy != null) {
                    enemy.setLevelScreen(levelScreen);
                }
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        period = jsonValue.getFloat("period", 0);

        last = jsonValue.getBoolean("last", false);

    }

}
