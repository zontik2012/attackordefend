package com.zontik2012.game.util;

/**
 * Variables to use in classes.
 */
public class Config {
    public static final int GAME_WIDTH = 1280;
    public static final int FIELD_WIDTH = 1080;
    public static final int GAME_HEIGHT = 720;
    public static final int CELL_W = 72;
    public static final int CELL_H = 72;
    public static final int GEM_SCREEN_GEM_SIZE = 100;
    public static final int DX[] = {0, 1, 0, -1};
    public static final int DY[] = {1, 0, -1, 0};
    public static double[] SIN;
    public static double[] COS;
    //map btns
    public static int[] LVL_BTN_X = {934, 1018, 960};
    public static int[] LVL_BTN_Y = {308, 420, 560};
    //gold at lvl
    public static final int TOWER_COST = 200;
    public static final int TOWER_UP_COST_DEF = 100;
    public static final int TOWER_DESTROY_GET_DEF = 100;
    public static final int LVL_GEM_COST = 200;
    public static final int LVL_GEM_DESTROY_GET_DEF=100;

}
