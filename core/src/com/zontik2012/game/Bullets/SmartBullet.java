package com.zontik2012.game.Bullets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.zontik2012.game.Enemies.AbstractEnemy;
import com.zontik2012.game.Screens.LevelScreen;
import com.zontik2012.game.Towers.Tower;
import com.zontik2012.game.util.Assets;
import com.zontik2012.game.util.Config;

public class SmartBullet {
    private AbstractEnemy target;
    private float x, y, tx, ty;
    private double deg, degAmount = 15;
    private int acc;
    private float vX, vY, maxSpeed = 216;
    private int damage;
    private Image image;
    private float distance;
    private boolean targetDead;
    private LevelScreen levelScreen;

    public SmartBullet(LevelScreen levelScreen, Tower tower, AbstractEnemy target, int damage, Color color) {
        this.target = target;
        x = tower.getPosX();
        y = tower.getPosY();
        double temp = Math.random();
        tx = target.getX();
        ty = target.getY();
        deg = MathUtils.atan2(target.getY() - y, target.getX() - x);
        if (temp > 0.5) {
            deg = deg + MathUtils.degreesToRadians * 45;
        } else {
            deg = deg - MathUtils.degreesToRadians * 45;
        }
        vX = (float) (Config.COS[(int) (deg * MathUtils.radiansToDegrees) % 360] * maxSpeed * 2);
        vY = (float) (Config.SIN[(int) (deg * MathUtils.radiansToDegrees) % 360] * maxSpeed * 2);
        acc = 18;
        this.damage = damage;
        image = new Image(levelScreen.getAssets().manager.get(Assets.smartBullet));
        image.setColor(color);
        image.setTouchable(Touchable.disabled);
        image.setWidth(36);
        image.setHeight(36);
        image.setPosition(x, y, Align.center);
        levelScreen.addActor(image);
        distance = (float) Math.sqrt((target.getWidth() * target.getWidth()) + (target.getHeight() * target.getHeight())) / 2;
        this.levelScreen = levelScreen;
    }

    public void update(float delta) {
        if (!targetDead) {
            tx = target.getX();
            ty = target.getY();
        }
        if (degAmount < 180)
            degAmount += 15 * delta;
        double tempDeg = MathUtils.atan2(ty - y, tx - x) - deg;
        if (tempDeg >= 0)
            deg += Math.min(tempDeg, MathUtils.degreesToRadians * degAmount);
        else
            deg += Math.max(tempDeg, -MathUtils.degreesToRadians * degAmount);

        double degT = deg * MathUtils.radiansToDegrees;
        if ((vX <= maxSpeed * Config.COS[(int) degT % 360] && Config.COS[(int) degT % 360] >= 0)
                || (vX >= maxSpeed * Config.COS[(int) degT % 360] && Config.COS[(int) degT % 360] <= 0))
            vX = (float) (vX + acc * Config.COS[(int) degT % 360]);

        if ((vY <= maxSpeed * Config.SIN[(int) degT % 360] && Config.SIN[(int) degT % 360] >= 0)
                || (vY >= maxSpeed * Config.SIN[(int) degT % 360] && Config.SIN[(int) degT % 360] <= 0)) {
            vY = (float) (vY + acc * Config.SIN[(int) degT % 360]);
        }
        x += vX * delta;
        y += vY * delta;
        image.setPosition(x, y, Align.center);

    }

    public AbstractEnemy getTarget() {
        return target;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public int getDamage() {
        return damage;
    }

    public boolean reachedTarget() {
        return Math.sqrt((tx - x) * (tx - x)
                + (ty - y) * (ty - y)) < distance;
    }

    public Image getImage() {
        return image;
    }

    public void dispose() {
        image.remove();
    }

    public void targetDied() {
        targetDead = true;
        target = null;

    }
}