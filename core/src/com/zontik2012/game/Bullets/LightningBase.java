package com.zontik2012.game.Bullets;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.zontik2012.game.Enemies.AbstractEnemy;
import com.zontik2012.game.Screens.LevelScreen;
import com.zontik2012.game.Towers.Tower;
import com.zontik2012.game.util.Assets;

public class LightningBase {
    private Image image;
    private AbstractEnemy target;
    private float x, y;
    private int damage;

    public LightningBase(LevelScreen levelScreen, Tower tower, AbstractEnemy target, int damage, Color color) {
        image = new Image(levelScreen.getAssets().manager.get(Assets.lightning));
        image.setColor(color);

        this.target = target;
        float sourceX = tower.getPosX();
        float sourceY = tower.getPosY();
        x = tower.getPosX();
        y = tower.getPosY();

        float rotateValue = (float) Math.atan2(sourceY - target.getY(), sourceX - target.getX());

        this.damage = damage;
        x -= image.getWidth() / 2;
        double scale = Math.abs(Math.sqrt(Math.pow(sourceX - target.getX(), 2) + Math.pow(sourceY - target.getY(), 2)) / image.getHeight());
        image.setScale((float) scale);
        image.setOriginX(image.getWidth() / 2);
        image.setRotation((float) Math.toDegrees(rotateValue) + 90);
    }


    public void dispose() {
        image.remove();
    }

    public AbstractEnemy getTarget() {
        return target;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Image getImage() {
        return image;
    }

    public float getPosX() {
        return x;
    }

    public float getPosY() {
        return y;
    }
}
