package com.zontik2012.game.Bullets;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.zontik2012.game.Enemies.AbstractEnemy;
import com.zontik2012.game.Towers.Tower;

public class InstantHitBase {
    private Texture texture;
    private Image image;
    private AbstractEnemy target;
    private float sourceX, sourceY;
    private Tower source;
    private float x, y;
    private int damage;

    public InstantHitBase(Tower tower, AbstractEnemy target, int damage) {
        source = tower;
        this.target = target;
        this.sourceX = tower.getPosX();
        this.sourceY = tower.getPosY();
        x = tower.getPosX();
        y = tower.getPosY();

        float rotateValue = (float) (Math.atan2(sourceY - target.getY(), sourceX - target.getX()) * 180 / Math.PI) + 90;

        this.damage = damage;
        texture = new Texture("bullets/lightning_blue.png");
        double scale = Math.abs(Math.sqrt(Math.pow(sourceX - target.getX(), 2) + Math.pow(sourceY - target.getY(), 2)) / texture.getHeight());

        image = new Image(texture);
        image.setScale((float) scale);
        image.setOriginX(image.getWidth() / 2);
        image.rotateBy(rotateValue);
    }


    public void dispose() {
        texture.dispose();
    }

    public AbstractEnemy getTarget() {
        return target;
    }

    public Texture getTexture() {
        return texture;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Image getImage() {
        return image;
    }

    public float getPosX() {
        return x;
    }

    public float getPosY() {
        return y;
    }
}
