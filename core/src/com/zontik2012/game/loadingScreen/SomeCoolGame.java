package com.zontik2012.game.loadingScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Timer;
import com.zontik2012.game.loadingScreen.screen.LoadingScreen;

/**
 * @author Mats Svensson
 */
public class SomeCoolGame extends Game {

    /**
     * Holds all our assets
     */
    public AssetManager manager = new AssetManager();

    @Override
    public void create() {
        setScreen(new LoadingScreen(this));
    }
}
