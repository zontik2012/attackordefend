package com.zontik2012.game.Field;

/**
 * Created by Сестра Бога on 02.12.2017.
 */

public class RoadCell extends FieldCell {


    public static final int DIRECTION_UP = 0;
    public static final int DIRECTION_RIGHT = 1;
    public static final int DIRECTION_DOWN = 2;
    public static final int DIRECTION_LEFT = 3;

    private boolean isStart;
    private boolean isEnd;


    private int direction = -1;


    @Override
    public int getType() {
        return TYPE_ROAD;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean start) {
        this.isStart = start;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
}
