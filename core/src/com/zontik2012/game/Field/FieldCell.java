package com.zontik2012.game.Field;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Scaling;
import com.zontik2012.game.util.Config;


public class FieldCell extends Image {
    public static final int TYPE_GROUND = 0;
    public static final int TYPE_ROAD = 1;
    public static final int TYPE_WATER = 2;
    public static final int TYPE_TREES = 3;
    public static final int TYPE_CROSSROAD = 4;


    public FieldCell() {
        super();
        resize();
    }

    public FieldCell(NinePatch patch) {
        super(patch);
        resize();
    }

    public FieldCell(TextureRegion region) {
        super(region);
        resize();
    }

    public FieldCell(Skin skin, String drawableName) {
        super(skin, drawableName);
        resize();
    }

    public FieldCell(Drawable drawable) {
        super(drawable);
        resize();
    }

    public FieldCell(Drawable drawable, Scaling scaling) {
        super(drawable, scaling);
        resize();
    }

    public FieldCell(Drawable drawable, Scaling scaling, int align) {
        super(drawable, scaling, align);
        resize();
    }

    public FieldCell(Texture texture) {
        super(texture);
        resize();
    }

    @Override
    public void setDrawable(Skin skin, String drawableName) {
        super.setDrawable(skin, drawableName);
        resize();
    }

    @Override
    public void setDrawable(Drawable drawable) {
        super.setDrawable(drawable);
        resize();
    }

    private void resize() {
        setSize(Config.CELL_W, Config.CELL_H);
    }

    public int getType() {
        return -1;
    }
}
