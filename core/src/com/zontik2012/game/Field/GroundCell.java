package com.zontik2012.game.Field;

/**
 * Created by Сестра Бога on 02.12.2017.
 */

public class GroundCell extends FieldCell {

    private boolean haveTower;

    @Override
    public int getType() {
        return TYPE_GROUND;
    }

    public boolean isHaveTower() {
        return haveTower;
    }

    public void setHaveTower(boolean haveTower) {
        this.haveTower = haveTower;
    }
}
