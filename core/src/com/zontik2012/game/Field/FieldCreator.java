package com.zontik2012.game.Field;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.zontik2012.game.Screens.LevelScreen;
import com.zontik2012.game.util.Assets;
import com.zontik2012.game.util.BuildListener;
import com.zontik2012.game.util.Config;
import com.zontik2012.game.util.Log;

import java.util.Random;

public class FieldCreator {
    private static FieldCell[][] fieldCells;
    private float stateTime = 0;
    private Image[][] water;
    private Animation<Image> waterAnim;
    private String levelString;
    private int startX, startY;
    private LevelScreen levelScreen;
    private Assets assets;

    public FieldCreator(LevelScreen levelScreen) {
        fieldCells = new FieldCell[15][10];
        for (int i = 0; i < fieldCells.length; i++) {
            for (int j = 0; j < fieldCells[i].length; j++) {
                fieldCells[i][j] = new FieldCell();
            }
        }
        this.levelScreen = levelScreen;
        levelString = levelScreen.getMap();
//        System.out.println(levelString);
        assets = levelScreen.getAssets();
        readLevel();
    }

    public FieldCell[][] getFieldCells() {
        return fieldCells;
    }

    private void readLevel() {
        String[] levelStrings = new String[10];
        for (int j = 9; j >= 0; j--) {
            levelStrings[j] = levelString.substring(0, 15);
            if (j != 0)
                levelString = levelString.substring(17, levelString.length());

        }
        for (int i = 0; i < 15; i++) {
            //for (int i = 9; i >= 0; i--) {;
            for (int j = 0; j < 10; j++) {
                switch (levelStrings[j].charAt(i)) {
                    //Клетка старта
                    case 'S':
                        fieldCells[i][j] = new RoadCell();
                        ((RoadCell) fieldCells[i][j]).setStart(true);
                        startX = i;
                        startY = j;
                        break;
                    //Дорога
                    case 'R':
                        fieldCells[i][j] = new RoadCell();
                        break;
                    //Просто земля
                    case '-':
                        fieldCells[i][j] = new GroundCell();
                        break;
                    //Вода
                    case '0':
                        fieldCells[i][j] = new WaterCell();
                        break;
                    //Деревья
                    case 'T':
                        fieldCells[i][j] = new WaterCell();
                        break;
                    //Конечная клетка
                    case 'E':
                        fieldCells[i][j] = new RoadCell();
                        ((RoadCell) fieldCells[i][j]).setEnd(true);
                        break;
                    default:
                        Log.logD("wrong char: " + "\"" + levelStrings[j].charAt(i) + "\"");
                        break;
                }
            }
        }
    }

    public void setLvlCells() {
        //setDirections(startX, startY);
        //Вода на заднем плане
        Array<Image> frames = new Array<Image>();
        TextureAtlas atlas = assets.manager.get(Assets.water);
        for (int k = 0; k < atlas.getRegions().size; k++)
            frames.add(new Image(atlas.getRegions().get(k)));
        waterAnim = new Animation<Image>(0.1f, frames);
        waterAnim.setPlayMode(Animation.PlayMode.LOOP);
        water = new Image[Config.GAME_WIDTH / (int) waterAnim.getKeyFrame(0).getWidth() + 1][Config.GAME_HEIGHT / (int) waterAnim.getKeyFrame(0).getHeight() + 1];

        for (int i = 0; i < water.length; i++) {
            for (int j = 0; j < water[i].length; j++) {
                Image result = new Image(waterAnim.getKeyFrame(stateTime).getDrawable());
                water[i][j] = result;
                result.setPosition(i * result.getWidth(), j * result.getHeight());
                result.setTouchable(Touchable.disabled);
                levelScreen.addActor(result);
            }
        }
        //Конец воды :3


        for (int i = 0; i < fieldCells.length; i++) {
            for (int j = 0; j < fieldCells[i].length; j++) {
                levelScreen.addActor(fieldCells[i][j]);
                String tempStr;

                if (fieldCells[i][j].getType() == FieldCell.TYPE_ROAD) {
                    tempStr = getRoadCell(i, j, (startX == j && startY == i));
                    FieldCell road = fieldCells[i][j];
                    road.setDrawable(assets.manager.get(Assets.roadSkin).getDrawable("road_" + (new Random().nextInt(2) + 1)));
                    road.setPosition(i * Config.CELL_W, j * Config.CELL_H);
                    Image coast = new Image(assets.manager.get(Assets.lightGrassCoastAtlas).findRegion(tempStr));
                    coast.setSize(Config.CELL_W, Config.CELL_H);
                    coast.setPosition(i * Config.CELL_W, j * Config.CELL_H);
                    coast.setTouchable(Touchable.disabled);
                    levelScreen.addActor(coast);

                }

                if (fieldCells[i][j].getType() == FieldCell.TYPE_WATER) {
                    tempStr = getWaterCell(i, j);
                    if (!tempStr.equals("11111111")) {
                        Image coast;
                        if (tempStr.equals(""))
                            coast = new Image(assets.manager.get(Assets.lightGrassCoastAtlas).findRegion("0"));
                        else
                            coast = new Image(assets.manager.get(Assets.lightGrassCoastAtlas).findRegion(tempStr));
                        coast.setSize(Config.CELL_W, Config.CELL_H);
                        coast.setPosition(i * Config.CELL_W, j * Config.CELL_H);
                        coast.setTouchable(Touchable.disabled);
                        levelScreen.addActor(coast);
                    }
                    //i-ширина, j-длина
                }
                if (fieldCells[i][j].getType() == FieldCell.TYPE_GROUND) {
                    String groundVariant = "";
                    int r1 = new Random().nextInt(100) + 1;
                    int grassInd = 1;
                    if (r1 > 0 && r1 <= 60) {
                        groundVariant = "common";
                        if (r1 > 0 && r1 <= 10) grassInd = 1;
                        if (r1 > 10 && r1 <= 20) grassInd = 2;
                        if (r1 > 20 && r1 <= 30) grassInd = 3;
                        if (r1 > 30 && r1 <= 40) grassInd = 4;
                        if (r1 > 40 && r1 <= 50) grassInd = 5;
                        if (r1 > 50 && r1 <= 60) grassInd = 6;

                    } else if (r1 > 60 && r1 <= 90) {
                        groundVariant = "uncommon";
                        if (r1 > 60 && r1 <= 70) grassInd = 1;
                        if (r1 > 70 && r1 <= 80) grassInd = 2;
                        if (r1 > 80 && r1 <= 90) grassInd = 3;

                    } else if (r1 > 90 && r1 <= 100) {
                        groundVariant = "rare";
                        if (r1 > 90 && r1 <= 93) grassInd = 1;
                        if (r1 > 93 && r1 <= 96) grassInd = 2;
                        if (r1 > 96 && r1 <= 98) grassInd = 3;
                        if (r1 > 98 && r1 <= 100) grassInd = 4;

                    }
                    FieldCell grass = fieldCells[i][j];
                    grass.setDrawable(new TextureRegionDrawable(assets.manager.get(Assets.lightGrassAtlas).findRegion(groundVariant, grassInd)));
                    grass.setPosition(i * Config.CELL_W, j * Config.CELL_H);
                    grass.addListener(new BuildListener(levelScreen, i, j));
                }
                if (fieldCells[i][j].getType() == FieldCell.TYPE_ROAD && ((RoadCell) fieldCells[i][j]).isEnd()) {
                    Image result2 = new Image(assets.manager.get(Assets.portal));
                    result2.setSize(Config.CELL_W, Config.CELL_H);
                    result2.setPosition(i * Config.CELL_W, j * Config.CELL_H);
                    result2.setTouchable(Touchable.disabled);
                    levelScreen.addActor(result2);
                }
            }

        }
    }

    private String getWaterCell(int i, int j) {
        String result = "";

        if (i == 0 && j == 0) {
            //нижняя левая
            result = result + "1";
            result = result + "1";
            result = result + "1";
            result = result + "1";
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//5
            result = result + "1";
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//7
            if (fieldCells[i + 1][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//8

        } else if (j > 0 && j < 9 && i == 0) {
            //левый бок кроме верхней и нижней
            result = result + "1";
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//2
            if (fieldCells[i + 1][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//3
            result = result + "1";
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//5
            result = result + "1";
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//7
            if (fieldCells[i + 1][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//8


        } else if (j == 9 && i == 0) {
            //верхняя левая
            result = result + "1";
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//2
            if (fieldCells[i + 1][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//3
            result = result + "1";
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//5
            result = result + "1";
            result = result + "1";
            result = result + "1";

        } else if (j == 9 && i > 0 && i < 14) {
            //верхняя кроме левой и правой
            result = result + "1";
            result = result + "1";
            result = result + "1";
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//4
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//5
            if (fieldCells[i - 1][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//6
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//7
            if (fieldCells[i + 1][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//8

        } else if (j == 9 && i == 14) {
            //верхняя правая
            result = result + "1";
            result = result + "1";
            result = result + "1";
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//4
            result = result + "1";
            if (fieldCells[i - 1][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//6
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//7
            result = result + "1";

        } else if (j > 0 && j < 9 && i == 14) {
            //правый бок кроме верхней и нижней
            if (fieldCells[i - 1][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//1
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//2
            result = result + "1";
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//4
            result = result + "1";
            if (fieldCells[i - 1][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//6
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//7
            result = result + "1";

        } else if (j == 0 && i > 0 && i < 14) {
            //нижняя кроме левой и правой
            if (fieldCells[i - 1][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//1
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//2
            if (fieldCells[i + 1][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//3
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//4
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//5
            result = result + "1";
            result = result + "1";
            result = result + "1";

        } else if (j == 0 && i == 14) {
            //нижняя правая
            if (fieldCells[i - 1][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//1
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//2
            result = result + "1";
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//4
            result = result + "1";
            result = result + "1";
            result = result + "1";
            result = result + "1";

        } else if (j > 0 && j < 9 && i > 0 && i < 14) {
            //серединная часть
            if (fieldCells[i - 1][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//1
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//2
            if (fieldCells[i + 1][j + 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//3
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//4
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//5
            if (fieldCells[i - 1][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//6
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//7
            if (fieldCells[i + 1][j - 1].getType() == FieldCell.TYPE_WATER) result = result + "1";
            else result = result + "0";//8
        }
        return result;
    }

    private String getRoadCell(int i, int j, boolean isStart) {
        String result = "";
        if (j > 0 && j < 9 && i == 0) {
            //левый бок кроме верхней и нижней
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_ROAD) result = result + "010";
            else result = result + "000";
            if (isStart) result = result + "1";
            else
                result = result + "0";
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";

        } else if (i == 0 && j == 9) {
            //верхняя левая
            result = result + "0000";
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";

        } else if (j == 9 && i > 0 && i < 14) {
            //верхняя кроме левой и правой
            if (isStart) result = result + "010";
            else
                result = result + "000";
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "1";
            else result = result + "0";
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";

        } else if (j == 9 && i == 14) {
            //верхняя правая
            result = result + "000";
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "1";
            else result = result + "0";
            result = result + "00";
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";

        } else if (j > 0 && j < 9 && i == 14) {
            //правый бок кроме верхней и нижней
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_ROAD) result = result + "010";
            else result = result + "000";
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "1";
            else result = result + "0";
            if (isStart) result = result + "10";
            else
                result = result + "00";
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";

        } else if (j == 0 && i == 14) {
            //нижняя правая
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_ROAD) result = result + "010";
            else result = result + "000";
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "1";
            else result = result + "0";
            result = result + "0000";

        } else if (j == 0 && i > 0 && i < 14) {
            //нижняя кроме левой и правой
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_ROAD) result = result + "010";
            else result = result + "000";
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "1";
            else result = result + "0";
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";
            if (isStart) result = result + "10";
            else
                result = result + "00";

        } else if (j == 0 && i == 0) {
            //нижняя левая
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_ROAD) result = result + "010";
            else result = result + "000";
            result = result + "0";
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";
            result = result + "00";

        } else if (j > 0 && j < 9 && i > 0 && i < 14) {
            //серединная часть
            if (fieldCells[i][j + 1].getType() == FieldCell.TYPE_ROAD) result = result + "010";
            else result = result + "000";
            if (fieldCells[i - 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "1";
            else result = result + "0";
            if (fieldCells[i + 1][j].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";
            if (fieldCells[i][j - 1].getType() == FieldCell.TYPE_ROAD) result = result + "10";
            else result = result + "00";
        }
        return result;
    }

    public int getStartX() {
        return startX;
    }

    public int getStartY() {
        return startY;
    }

    public void update(float delta) {
        stateTime += delta;
        for (Image[] aWater : water) {
            for (Image anAWater : aWater) {
                anAWater.setDrawable(waterAnim.getKeyFrame(stateTime).getDrawable());
            }
        }
    }


}
