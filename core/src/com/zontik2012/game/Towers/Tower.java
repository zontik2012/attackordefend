package com.zontik2012.game.Towers;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.zontik2012.game.Enemies.AbstractEnemy;
import com.zontik2012.game.Gems.Gem;
import com.zontik2012.game.Gems.GemCell;
import com.zontik2012.game.Screens.GemScreen;
import com.zontik2012.game.Screens.LevelScreen;
import com.zontik2012.game.util.Assets;
import com.zontik2012.game.util.Config;

import java.util.ArrayList;

/**
 * Tower class. used in level, can contain gem and shoot if has one inside.
 */
public class Tower {

    private Image image;

    private ArrayList<AbstractEnemy> targets;
    private int posX;
    private int posY;
    private int gridX, gridY;
    private int rank = 1;

    private Gem gem;
    private boolean canShoot = true;

    private float speedMod;
    private float rangeMod;

    public Tower(int x, int y, Assets assets) {
        speedMod = 1;
        rangeMod = 1;
        targets = new ArrayList<AbstractEnemy>();
        posX = x * Config.CELL_W;
        posY = y * Config.CELL_H;
        gridX = x;
        gridY = y;
        image = new Image(assets.manager.get(Assets.tower));
        image.setSize(Config.CELL_W, Config.CELL_H);
    }

    public void updateFocus() {
        if (targets.size() > 0) {
            for (int i = 0; i < targets.size(); i++) {
                AbstractEnemy target = targets.get(i);
                if (Math.sqrt((getPosX() - target.getX()) * (getPosX() - target.getX()) + (getPosY() - target.getY()) * (getPosY() - target.getY())) >= getRange()) {
                    targets.remove(target);
                    i--;
                    if (targets.size() < 0) {
                        break;
                    }
                }
            }

        }
    }

    /**
     * @return y-position of place tower should stand on screen, centre align
     */
    public float getPosY() {
        return posY + image.getHeight() / 2;
    }

    /**
     * @return x-position of place tower should stand on screen,center align
     */
    public float getPosX() {
        return posX + image.getWidth() / 2;
    }

    public void addTarget(AbstractEnemy target) {
        targets.add(target);
        target.addFocusingTower(this);
    }

    public float getPeriod() {
        if (gem != null)
            return getGem().getCoolDown() * speedMod;
        else return 0;
    }

    public void putGem(Gem gem) {
        this.gem = gem;
        gem.getImage().setZIndex(image.getZIndex() + 1);
        gem.getImage().setSize(image.getWidth() * 0.65f, image.getHeight() * 0.65f);
        gem.getImage().setPosition(image.getX() + image.getWidth() / 2, image.getY() + image.getHeight() / 2, Align.center);
    }

    public Gem takeGem() {
        Gem result = gem;
        gem.getImage().remove();
        gem = null;
        return result;
    }

    /**
     * @return gem inside tower
     */
    public Gem getGem() {
        return gem;
    }

    public boolean hasGem() {
        return gem != null;
    }

    public ArrayList<AbstractEnemy> getTargets() {
        return targets;
    }

    public Image getImage() {
        return image;
    }

    public void upgrade() {
        rank++;
        speedMod *= 0.95;
        rangeMod *= 1.05;
    }

    public boolean getCanShoot() {
        return canShoot;
    }

    public void setCanShoot(boolean b) {
        canShoot = b;
    }

    public void lookForEnemies(ArrayList<AbstractEnemy> enemies) {
        float tx = getPosX(), ty = getPosY();
        AbstractEnemy enemy = null;
        for (int i = 0; i < enemies.size(); i++) {
            float ex = enemies.get(i).getX();
            float ey = enemies.get(i).getY();
            if (ex < Config.FIELD_WIDTH && ex > 0 && ey < Config.GAME_HEIGHT && ey > 0)
                if (Math.sqrt((tx - ex) * (tx - ex) + (ty - ey) * (ty - ey)) < getRange()) {
                    if (enemy == null)
                        enemy = enemies.get(i);
                    else if (Math.sqrt((tx - ex) * (tx - ex) + (ty - ey) * (ty - ey)) <
                            Math.sqrt((tx - enemy.getX()) * (tx - enemy.getX()) + (ty - enemy.getY()) * (ty - enemy.getY())))
                        enemy = enemies.get(i);
                }
        }
        if (enemy != null) {
            addTarget(enemy);
        }
    }

    public int getRange() {
        if (gem != null)
            return (int) (gem.getRange() * rangeMod);
        else return 0;
    }

    public int getRank() {
        return rank;
    }

    /**
     * @return grid x-position of tower
     */
    public int getGridX() {
        return gridX;
    }

    /**
     * @return grid y-position of tower
     */
    public int getGridY() {
        return gridY;
    }

    public void fromGemsToField(LevelScreen levelScreen, GemCell towerPlace) {
        towerPlace.getImage().remove();
        image.setSize(Config.CELL_W, Config.CELL_H);
        image.setPosition(posX, posY);
        levelScreen.addActor(image);
        if (towerPlace.hasGem()) {
            putGem(towerPlace.takeGem());
            levelScreen.addActor(gem.getImage());
            levelScreen.getUiManager().showRangeCircle();
        }
    }

    public void fromFieldToGems(GemScreen gemScreen) {
        image.setSize(200, 200);
        image.setPosition(GemScreen.TOWER_PLACE_X, GemScreen.TOWER_PLACE_Y, Align.topLeft);
        gemScreen.addActor(image);
        if (gem != null) {
            gem.getImage().setSize(image.getWidth() * 0.65f, image.getHeight() * 0.65f);
            gem.getImage().setPosition(image.getX() + image.getWidth() / 2, image.getY() + image.getHeight() / 2, Align.center);
            gemScreen.addActor(gem.getImage());
        }
    }
}
