package com.zontik2012.game.Managers;

import com.badlogic.gdx.math.MathUtils;
import com.zontik2012.game.MyGame;
import com.zontik2012.game.Screens.AbstractScreen;
import com.zontik2012.game.Screens.LevelScreen;
import com.zontik2012.game.Screens.LoadingScreen;
import com.zontik2012.game.Screens.MainMenuScreen;
import com.zontik2012.game.Screens.MapScreen;
import com.zontik2012.game.Screens.TestScreen;
import com.zontik2012.game.Screens.TutorialScreen;
import com.zontik2012.game.Towers.Tower;
import com.zontik2012.game.util.Assets;
import com.zontik2012.game.util.Config;

/**
 * Screen Manager. Unique, used to switch between screens.
 */
public class ScreenManager {

    // Singleton: unique instance
    private static ScreenManager instance;
    private Assets assets;
    // Reference to game
    private MyGame game;
    //LevelScreen when go to Wave
    private LevelScreen levelScreen;
    //LoadingScreen
    private LoadingScreen loadingScreen;


    private ScreenManager() {
    }

    public static ScreenManager getInstance() {
        if (instance == null) {
            instance = new ScreenManager();
        }
        return instance;
    }

    /**
     * Initialization with the game class
     */
    public void initialize(MyGame game) {
        this.game = game;
        assets = game.getAssets();
        assets.loadGame();
        loadingScreen = new LoadingScreen(assets);
        loadingScreen.buildStage();
        game.setScreen(loadingScreen);
        Config.SIN=new double[360];
        Config.COS=new double[360];
        for (int i=0;i<360;i++){
            Config.SIN[i]= MathUtils.sinDeg(i);
            Config.COS[i]= MathUtils.cosDeg(i);
        }
    }

    /**
     * Simple screen change
     */
    public void setScreen(AbstractScreen screen) {
        game.setScreen(screen);
    }

    /**
     * shows LoadingScreen
     */
    public void showLoading() {
        game.setScreen(loadingScreen);
    }

    /**
     * Shows Main Menu for the first time
     */
    public void startGame() {
        loadingScreen.startLoading(new MainMenuScreen(assets));
    }

    /**
     * Shows the test screen
     */
    public void startTest() {
        loadingScreen.startLoading(new TestScreen(assets),game.getScreen());
    }

    /**
     * Shows the map 2nd and further times
     */
    public void showMenu() {
        loadingScreen.startLoading(new MainMenuScreen(assets),game.getScreen());
    }

    /**
     * Shows map
     */
    public void showMap() {
        loadingScreen.startLoading(new MapScreen(assets),game.getScreen());
    }

    /**
     * Shows tutorial from Main Menu
     */
    public void showTutorial() {
        loadingScreen.startLoading(new TutorialScreen(assets),game.getScreen());
    }

    /**
     * Starts new level and setups new LevelScreen and GemScreen
     *
     * @param levelNum ehm...do you need an explanation?)
     */
    public void startLevel(int levelNum) {
        loadingScreen.startLoading(new LevelScreen(levelNum, assets),game.getScreen());
    }

    /**
     * Shows levelScreen from gemScreen
     */
    public void backFromGems() {
        game.setScreen(levelScreen);
    }

    /**
     * Shows gemScreen from levelScreen
     */
    public void showGemScreen(Tower tower) {
        game.setScreen(levelScreen.getGemScreen());
        levelScreen.getGemScreen().update(tower);
    }

    public LevelScreen getLevelScreen() {
        return levelScreen;
    }

    public void setLevelScreen(LevelScreen levelScreen) {
        this.levelScreen = levelScreen;
    }

    public LoadingScreen getLoadingScreen() {
        return loadingScreen;
    }
}
