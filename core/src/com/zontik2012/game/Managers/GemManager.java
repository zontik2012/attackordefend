package com.zontik2012.game.Managers;

import com.badlogic.gdx.Gdx;
import com.zontik2012.game.Gems.Gem;
import com.zontik2012.game.Towers.Tower;

import java.util.ArrayList;
import java.util.Random;

/**
 * Manager class for gems. Unique, recreating on level start.
 */
public class GemManager {
    private static GemManager ourInstance;
    private ArrayList<Gem> gems = new ArrayList<Gem>();

    private GemManager(int amount) {
        for (int i = 0; i < amount; i++) {
            Gem gem = createNewGem();
            gems.add(gem);
        }

    }

    public static GemManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new GemManager(10);
        }
        return ourInstance;
    }

    public static void reset(int amount) {
        ourInstance = new GemManager(amount);
    }

    public Gem createNewGem() {
        Gem result;
        switch (new Random().nextInt(4) + 1) {
            case 1:
                result = new Gem(100, 1f, 1, 1, 2, 1);
                break;
            case 2:
                result = new Gem(70, 1, 1, 1, 3, 2);
                break;
            case 3:
                result = new Gem(150, 2, 2, 1, 1, 3);
                break;
            case 4:
                result = new Gem(65, 1, 1, 1, 1, 4);
                break;
            default:
                result = new Gem(1, 1, 1, 1, 1, 1);
                Gdx.app.debug("MyError", "Wrong Gem Random");
        }
        return result;
    }

    public void addNewGem() {
        gems.add(createNewGem());
    }

    public Gem mergeGems(Gem gem1, Gem gem2) {
        int selectFrom = new Random().nextInt(2);
        int element;
        int type;
        float range;
        int dmg = (int) ((gem1.getDamage() + gem2.getDamage()) / 1.8);
        int level = Math.min(gem1.getLevel() + gem2.getLevel(), 2);
        int rank = Math.max(gem1.getRank(), gem2.getRank());
        float cd = (gem1.getCoolDown() + gem2.getCoolDown()) / 2.05f;
        if (selectFrom == 1) {
            element = gem1.getElement();
            type = gem1.getAttackType();
            range = gem1.getRange() * 1.05f;
        } else {
            element = gem2.getElement();
            type = gem2.getAttackType();
            range = gem2.getRange() * 1.05f;
        }
        int targets = Math.max(gem1.getTargetsCount(), gem2.getTargetsCount());
        Gem result = new Gem(dmg, cd, type, targets, (int) range, element, level);
        return result;
    }

    public void addGem(Gem gem) {
        gems.add(gem);
    }


    public ArrayList<Gem> getGems() {
        return gems;
    }

    public void takeGem(Tower tower) {
        gems.add(tower.takeGem());
    }

    public void removeGem(Gem gem) {
        gem.getImage().remove();
        gems.remove(gem);
    }
}
