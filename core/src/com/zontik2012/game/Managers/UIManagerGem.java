package com.zontik2012.game.Managers;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.zontik2012.game.Gems.Gem;
import com.zontik2012.game.Gems.GemCell;
import com.zontik2012.game.Screens.GemScreen;
import com.zontik2012.game.Screens.LevelScreen;
import com.zontik2012.game.Towers.Tower;
import com.zontik2012.game.util.Config;

/**
 * Manager for GemScreen. UI things and etc, just like UIManagerLevel
 */
public class UIManagerGem {


    private static final int DIALOG_CREATE = 1, DIALOG_SELL = 2, DIALOG_MERGE = 3;
    private int CURRENT_DIALOG;
    private Skin uiSkin;
    private TextButton.TextButtonStyle textButtonStyle;
    private GemScreen gemScreen;
    private LevelScreen levelScreen;
    private Dialog confirmDialog;
    private Timer dialogTimer;
    private Label dialogDescription;
    private TextButton yesDeal;
    private DragAndDrop gemDragDrop;
    private GemCell[][] gemCells;
    private GemCell towerPlace;
    private Gem soldGem;
    private TextButton backBtn;

    public UIManagerGem(GemScreen gemScreen) {
        this.gemScreen = gemScreen;
        this.levelScreen = gemScreen.getLevelScreen();
        gemDragDrop = new DragAndDrop();
        gemCells = gemScreen.getGemCells();
        towerPlace = gemScreen.getTowerPlace();
        uiSkin = gemScreen.getSkin();
        textButtonStyle = uiSkin.optional("mediumButton30", TextButton.TextButtonStyle.class);
        backBtn = new TextButton("Назад", textButtonStyle);
        backBtn.setPosition(15, 15);
        gemScreen.addActor(backBtn);
        for (GemCell[] gemCell1 : gemCells) {
            for (final GemCell gemCell : gemCell1) {
                gemScreen.addActor(gemCell.getImage());
            }
        }
        update();

    }

    public void update() {

        Tower tempTower = gemScreen.getDisplayedTower();
        if (tempTower != null) {
            gemScreen.addActor(towerPlace.getImage());
            if (tempTower.hasGem()) towerPlace.putGem(tempTower.takeGem());
        }

        gemDragDrop.clear();
        gemDragDrop.setDragActorPosition(Config.GAME_HEIGHT / 7.f / 2, -Config.GAME_HEIGHT / 7.f / 2);
        gemDragDrop.setTapSquareSize(0);
        gemDragDrop.setDragTime(0);
        //Сетка
        for (GemCell[] gemCell1 : gemCells) {
            for (final GemCell gemCell : gemCell1) {
                if (gemCell.hasGem()) {
                    gemScreen.addActor(gemCell.getGem().getImage());
                    gemCell.getGem().getImage().setTouchable(Touchable.disabled);
                    gemDragDrop.addSource(new DragAndDrop.Source(gemCell.getImage()) {
                        @Override
                        public void dragStop(InputEvent event, float x, float y, int pointer, DragAndDrop.Payload payload, DragAndDrop.Target target) {
                            if (payload != null)
                                gemScreen.addActor(((Gem) payload.getObject()).getImage());
                        }

                        @Override
                        public DragAndDrop.Payload dragStart(InputEvent event, float x, float y, int pointer) {
                            DragAndDrop.Payload payload = new DragAndDrop.Payload();
                            payload.setObject(gemCell.getGem());
                            gemCell.getGem().getImage().remove();
                            Image payloadActor = new Image(gemCell.getGem().getImage().getDrawable());
                            payloadActor.setSize(gemCell.getGem().getImage().getWidth(), gemCell.getGem().getImage().getHeight());
                            payloadActor.setColor(gemCell.getGem().getGemColor());
                            payload.setDragActor(payloadActor);
                            return payload;
                        }
                    });
                    gemDragDrop.addTarget(new DragAndDrop.Target(gemCell.getImage()) {
                        @Override
                        public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                            return true;
                        }

                        @Override
                        public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                            final GemCell aGemCell = findCell((Gem) payload.getObject());
                            if (gemCell != aGemCell) {
                                createMergeDialogAtMove(aGemCell, gemCell);
                            }
                        }
                    });
                } else {
                    gemDragDrop.addTarget(new DragAndDrop.Target(gemCell.getImage()) {

                        @Override
                        public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                            return true;
                        }

                        @Override
                        public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                            gemCell.putGem(findCell((Gem) payload.getObject()).takeGem());
                            update();
                        }
                    });

                }
            }
        }

        //Продажа
        gemDragDrop.addTarget(new DragAndDrop.Target(gemScreen.getSellPlace()) {
            @Override
            public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                return true;
            }

            @Override
            public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                CURRENT_DIALOG = DIALOG_SELL;
                soldGem = (Gem) payload.getObject();
                makeTransp(soldGem);
                showDealDialog(Config.LVL_GEM_DESTROY_GET_DEF * soldGem.getLevel());
            }
        });

        //Башня
        if (gemScreen.getDisplayedTower() != null) {
            if (towerPlace.hasGem()) {
                gemDragDrop.addSource(new DragAndDrop.Source(towerPlace.getImage()) {
                    @Override
                    public void dragStop(InputEvent event, float x, float y, int pointer, DragAndDrop.Payload payload, DragAndDrop.Target target) {
                        if (payload != null)
                            gemScreen.addActor(((Gem) payload.getObject()).getImage());
                    }

                    @Override
                    public DragAndDrop.Payload dragStart(InputEvent event, float x, float y, int pointer) {
                        DragAndDrop.Payload payload = new DragAndDrop.Payload();
                        payload.setObject(towerPlace.getGem());
                        towerPlace.getGem().getImage().remove();
                        Image payloadActor = new Image(towerPlace.getGem().getImage().getDrawable());
                        payloadActor.setSize(towerPlace.getGem().getImage().getWidth(), towerPlace.getGem().getImage().getHeight());
                        payloadActor.setColor(towerPlace.getGem().getGemColor());
                        payload.setDragActor(payloadActor);
                        return payload;
                    }
                });
                gemDragDrop.addTarget(new DragAndDrop.Target(towerPlace.getImage()) {
                    @Override
                    public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                        return true;
                    }

                    @Override
                    public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                        final GemCell aGemCell = findCell((Gem) payload.getObject());
                        if (towerPlace != aGemCell) {
                            createMergeDialogAtMove(aGemCell, towerPlace);
                        }
                    }
                });
            } else {
                gemDragDrop.addTarget(new DragAndDrop.Target(towerPlace.getImage()) {
                    @Override
                    public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                        return true;
                    }

                    @Override
                    public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                        towerPlace.putGem(findCell((Gem) payload.getObject()).takeGem());
                        update();
                    }
                });
            }
        }
    }

    public void setUpDialogs() {
        dialogTimer = new Timer();
        confirmDialog = new Dialog("", uiSkin);
        dialogDescription = new Label("", uiSkin.optional("dialogText", Label.LabelStyle.class));
        dialogDescription.setSize(800, 250);
        dialogDescription.setAlignment(Align.center);
        confirmDialog.text(dialogDescription);

        TextButton.TextButtonStyle textButtonStyle = uiSkin.optional("dialogBtn", TextButton.TextButtonStyle.class);
        confirmDialog.getButtonTable().pad(10, 75, 10, 75);
        yesDeal = new TextButton("Да", textButtonStyle);
        yesDeal.align(Align.center);
        yesDeal.setSize(250, 80);
        yesDeal.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                switch (CURRENT_DIALOG) {
                    case DIALOG_CREATE: {
                        dialogTimer.scheduleTask(new Timer.Task() {
                            @Override
                            public void run() {
                                levelScreen.takeGold(Config.LVL_GEM_COST);
                                GemManager.getInstance().addNewGem();
                                update();
                            }
                        }, 0.4f);
                        break;
                    }
                    case DIALOG_SELL: {
                        dialogTimer.scheduleTask(new Timer.Task() {
                            @Override
                            public void run() {
                                levelScreen.addGold(Config.LVL_GEM_DESTROY_GET_DEF * soldGem.getLevel());
                                findCell(soldGem).takeGem();
                                GemManager.getInstance().removeGem(soldGem);
                                update();
                            }
                        }, 0.4f);

                        break;
                    }

                }
                confirmDialog.hide();
            }
        });

        TextButton noDeal = new TextButton("Нет", textButtonStyle);
        noDeal.setSize(250, 80);
        noDeal.align(Align.center);
        confirmDialog.button(yesDeal);
        confirmDialog.button(noDeal);
        noDeal.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                unMakeTransp(soldGem);
            }
        });


        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().backFromGems();
                if (gemScreen.getDisplayedTower() != null) {
                    gemScreen.getDisplayedTower().fromGemsToField(levelScreen, towerPlace);
                }
                levelScreen.unpauseLevel();
            }

        });
    }

    private void showDealDialog(int gold) {
        switch (CURRENT_DIALOG) {
            case DIALOG_CREATE: {
                dialogDescription.setText("Вы уверены, что хотите создать камень?" +
                        "\nЭто будет стоить " + gold + " золота.");
                break;
            }
            case DIALOG_SELL: {
                dialogDescription.setText("Вы уверены, что хотите разрушить камень?" +
                        "\nЭто принесёт вам " + gold + " золота.");

                break;
            }
        }
        if (levelScreen.getGold() >= gold || CURRENT_DIALOG == DIALOG_SELL) {
            yesDeal.setText("Да");
            yesDeal.setDisabled(false);
            yesDeal.setColor(1, 1, 1, 1);
            yesDeal.setTouchable(Touchable.enabled);
        } else {
            yesDeal.setText("Недостаточно золота!");
            yesDeal.setDisabled(true);
            yesDeal.setColor(0.5f, .5f, .5f, 1);
            yesDeal.setTouchable(Touchable.disabled);
        }
        confirmDialog.show(gemScreen);
    }

    private void makeTransp(Gem gem) {
        gem.getImage().setColor(gem.getGemColor().r, gem.getGemColor().g, gem.getGemColor().b, 0);
    }

    private void unMakeTransp(Gem gem) {
        gem.getImage().setColor(gem.getGemColor().r, gem.getGemColor().g, gem.getGemColor().b, 1);

    }

    private GemCell findCell(Gem gem) {
        if (gem == towerPlace.getGem()) return towerPlace;
        else
            for (GemCell[] gemCelll : gemCells) {
                for (GemCell aGemCell : gemCelll) {
                    if (gem == aGemCell.getGem()) return aGemCell;
                }
            }
        return null;
    }

    private void createMergeDialogAtMove(final GemCell from, final GemCell to) {
        makeTransp(from.getGem());

        Dialog dialog = new Dialog("Слияние Камней", uiSkin);
        Label desc = new Label("ВЫ УВЕРЕНЫ, ЧТО ХОТИТЕ ПРОВЕСТИ СЛИЯНИЕ?" +
                "\nИСПОЛЬЗОВАННЫЕ КАМНИ ИСЧЕЗНУТ!", uiSkin.optional("dialogText", Label.LabelStyle.class));
        desc.setSize(800, 250);
        desc.setAlignment(Align.center);
        dialog.text(desc);

        TextButton.TextButtonStyle btnStyle = uiSkin.optional("dialogBtn", TextButton.TextButtonStyle.class);
        dialog.getButtonTable().pad(10, 75, 10, 75);
        TextButton yes = new TextButton("Да", btnStyle);
        yes.align(Align.center);
        yes.setSize(250, 80);
        yes.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        to.putGem(GemManager.getInstance().mergeGems(from.takeGem(), to.takeGem()));
                        update();
                    }
                }, 0.4f);
            }
        });

        TextButton no = new TextButton("Нет", btnStyle);
        no.setSize(250, 80);
        no.align(Align.center);
        no.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                unMakeTransp(from.getGem());
            }
        });
        dialog.button(yes);
        dialog.button(no);
        dialog.show(gemScreen);
    }

}
