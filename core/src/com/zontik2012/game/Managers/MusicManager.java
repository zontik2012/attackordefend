package com.zontik2012.game.Managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.utils.Timer;
import com.zontik2012.game.util.Assets;


public class MusicManager {

    private static final MusicManager ourInstance = new MusicManager();
    private Music currentMusic;
    private boolean muted;
    private Timer switchTimer;
    private Assets assets;

    private MusicManager() {
        muted = Gdx.app.getPreferences("settings").getBoolean("muted", false);
        switchTimer = new Timer();
    }

    public static MusicManager getInstance() {
        return ourInstance;
    }

    public void startMusicOnLaunch(Assets assets) {
        this.assets = assets;
        currentMusic = assets.manager.get(Assets.menuMusic);
        currentMusic.setLooping(true);
        if (muted) currentMusic.stop();
        else currentMusic.play();
    }

    public void switchTo(AssetDescriptor<Music> musicDesc) {
        final Music music = assets.manager.get(musicDesc);
        music.setLooping(true);
        if (muted) {
            currentMusic = music;
        } else {
            currentMusic.stop();
            music.play();
            currentMusic = music;
        }
    }

    public void switchMute() {
        if (muted)
            currentMusic.play();
        else
            currentMusic.stop();
        muted = !muted;
        Gdx.app.getPreferences("settings").putBoolean("muted", muted);
        Gdx.app.getPreferences("settings").flush();
    }

    public String getSndString() {
        if (muted) return "Выкл";
        else return "Вкл";
    }

    public boolean isMuted(){
        return muted;
    }

    public void stopOnPause(){
        currentMusic.pause();
    }

    public void playOnResume(){
        currentMusic.play();
    }

    public void setAssets(Assets assets) {
        this.assets = assets;
    }
}
