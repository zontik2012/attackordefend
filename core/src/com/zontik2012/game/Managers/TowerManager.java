package com.zontik2012.game.Managers;


import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Timer;
import com.zontik2012.game.Bullets.LightningBase;
import com.zontik2012.game.Bullets.SmartBullet;
import com.zontik2012.game.Enemies.AbstractEnemy;
import com.zontik2012.game.Field.GroundCell;
import com.zontik2012.game.Screens.LevelScreen;
import com.zontik2012.game.Towers.Tower;
import com.zontik2012.game.util.Config;

import java.util.ArrayList;

public class TowerManager {

    private LevelScreen levelScreen;
    private Timer shotCdTimer = new Timer();
    private ArrayList<SmartBullet> smartBullets = new ArrayList<SmartBullet>();
    private ArrayList<LightningBase> lightningBases = new ArrayList<LightningBase>();
    private Tower[][] towers = new Tower[15][10];

    public TowerManager(LevelScreen levelScreen) {
        this.levelScreen = levelScreen;
    }

    public Tower[][] getTowers() {
        return towers;
    }


    public void count() {
        int count = 0;
        for (int x = 0; x < 15; x++) {
            for (int y = 0; y < 10; y++) {
                if (towers[x][y] != null) count++;
            }

        }
        System.out.println(count);

    }

    public void buildTower(int x, int y) {
        Tower tower = new Tower(x, y, levelScreen.getAssets());
        towers[x][y] = tower;
        levelScreen.addActor(tower.getImage());
        tower.getImage().setPosition(x * Config.CELL_W, y * Config.CELL_H);
        tower.getImage().setTouchable(Touchable.disabled);
        ((GroundCell) levelScreen.getField()[x][y]).setHaveTower(true);
    }

    private void upgradeTower(int x, int y) {
        towers[x][y].upgrade();
    }

    public void upgradeTower(Tower tower) {
        upgradeTower(tower.getGridX(), tower.getGridY());
    }

    public void removeTower(Tower tower) {
        levelScreen.getUiManager().deselectCell();
        tower.getImage().remove();
        ((GroundCell) levelScreen.getField()[tower.getGridX()][tower.getGridY()]).setHaveTower(false);
        towers[tower.getGridX()][tower.getGridY()] = null;
        if (tower.hasGem())
            for (int i = 0; i < levelScreen.getGemScreen().getGemCells().length; i++)
                for (int j = 0; j < levelScreen.getGemScreen().getGemCells()[i].length; j++)
                    if (!levelScreen.getGemScreen().getGemCells()[i][j].hasGem())
                        levelScreen.getGemScreen().getGemCells()[i][j].putGem(tower.takeGem());
    }

    public void updateAndShoot(ArrayList<AbstractEnemy> enemies) {
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 10; j++) {
                if (towers[i][j] != null) {
                    final Tower tower = towers[i][j];
                    if (tower.hasGem()) {
                        tower.updateFocus();
                        if (tower.getTargets().size() < tower.getGem().getTargetsCount()) {
                            tower.lookForEnemies(enemies);
                        } else {
                            if (tower.getCanShoot()) {
                                for (int k = 0; k < tower.getTargets().size(); k++) {
                                    switch (tower.getGem().getAttackType()) {
                                        case 1:
                                            SmartBullet bullet = new SmartBullet(levelScreen, tower, tower.getTargets().get(k), tower.getGem().getDamage(), tower.getGem().getGemColor());
                                            smartBullets.add(bullet);
                                            tower.getTargets().get(k).addFocusingSmartBullet(bullet);
                                            break;
                                        case 2:
                                            final LightningBase instantHitBase = new LightningBase(levelScreen, tower, tower.getTargets().get(k), tower.getGem().getDamage(), tower.getGem().getGemColor());
                                            lightningBases.add(instantHitBase);
                                            levelScreen.addActor(instantHitBase.getImage());
                                            instantHitBase.getImage().setPosition(instantHitBase.getPosX(), instantHitBase.getPosY());
                                            shotCdTimer.scheduleTask(new Timer.Task() {
                                                @Override
                                                public void run() {
                                                    instantHitBase.getImage().remove();
                                                    instantHitBase.dispose();
                                                }
                                            }, 0.2f);

                                            tower.getTargets().get(k).damage(instantHitBase.getDamage());
                                            break;
                                    }
                                }
                                tower.setCanShoot(false);
                                shotCdTimer.scheduleTask(new Timer.Task() {
                                    @Override
                                    public void run() {
                                        tower.setCanShoot(true);
                                    }
                                }, tower.getPeriod());
                            }
                        }
                    }
                }
            }
        }

    }

    public void moveBullets(float delta) {

        for (int i = 0; i < smartBullets.size(); i++) {
            SmartBullet bullet = smartBullets.get(i);
            bullet.update(delta);
            if (bullet.reachedTarget()) {
                if (bullet.getTarget() != null)
                    bullet.getTarget().damage(bullet.getDamage());
                bullet.dispose();
                smartBullets.remove(i);
                i--;
            }
        }
    }

    public Timer getShotCdTimer() {
        return shotCdTimer;
    }

    public ArrayList<SmartBullet> getSmartBullets() {
        return smartBullets;
    }

    public void reset() {
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 10; j++) {
                Tower tower = towers[i][j];
                if (tower != null) {
                    removeTower(tower);
                }
            }
        }

        for (int i = 0; i < smartBullets.size(); i++) {
            smartBullets.get(i).dispose();
        }
        smartBullets.clear();
        shotCdTimer.clear();
    }
}
