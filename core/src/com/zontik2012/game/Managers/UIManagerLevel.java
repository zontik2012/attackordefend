package com.zontik2012.game.Managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.zontik2012.game.Enemies.AbstractEnemy;
import com.zontik2012.game.Field.FieldCell;
import com.zontik2012.game.Field.GroundCell;
import com.zontik2012.game.Screens.LevelScreen;
import com.zontik2012.game.Towers.Tower;
import com.zontik2012.game.util.Assets;
import com.zontik2012.game.util.Config;
import com.zontik2012.game.util.Wave;


/**
 * Manager for Level.Contains almost all UI things and some more.
 */
public class UIManagerLevel {
    public static final int DIALOG_BUILD = 1;
    public static final int DIALOG_UPGRADE = 2;
    public static final int DIALOG_SELL = 3;

    public static final int DIALOG_QUIT = 4;

    public static int CURRENT_DIALOG;
    private TextButton sellBtn;
    private TextButton buildBtn;
    private TextButton gemBtn;
    private TextButton upgradeTowerBtn;
    private TextButton backToMapBtn;
    private TextButton startWaveBtn;
    private Table sideBar;
    private Table goldLives;
    private Table towerStats;
    //private Table heartsBar;
    private Texture gold;
    private Texture lives;
    private Table buttons;
    private Skin uiSkin;
    private TextButton.TextButtonStyle buttonStyle;
    private Texture select = new Texture(Gdx.files.internal("UI/other/tower_select.png"));
    private Image selectImg = new Image(select);
    private boolean selected, confirmedDeal = false;
    private Wave wave;
    private LevelScreen levelScreen;
    private Texture rangeTxtr;
    private Image rangeCircle;
    private Dialog winDialog, loseDialog, dealDialog, confirmDialog;
    private TextButton yesDeal;
    private Label dealDescription;
    private Timer dealTimer;
    private Tower selectedCell;
    public UIManagerLevel(LevelScreen levelScreen) {
        this.levelScreen = levelScreen;
        uiSkin = levelScreen.getAssets().manager.get(Assets.uiSkin);
        gold = new Texture(Gdx.files.internal("UI/icons/coins.png"));
        lives = new Texture(Gdx.files.internal("UI/icons/hearts.png"));

        buttonStyle = uiSkin.optional("mediumButton36", TextButton.TextButtonStyle.class);

        selectImg.setSize(Config.CELL_W, Config.CELL_H);
        selectImg.setTouchable(Touchable.disabled);

        backToMapBtn = new TextButton("Карта", buttonStyle);
        buildBtn = new TextButton("Построить", buttonStyle);
        sellBtn = new TextButton("Продать", buttonStyle);
        upgradeTowerBtn = new TextButton("Улучшить", buttonStyle);
        gemBtn = new TextButton("Камни", buttonStyle);
        startWaveBtn = new TextButton("Волна", buttonStyle);


        sideBar = new Table(uiSkin);
        sideBar.setPosition(Config.FIELD_WIDTH, 0);
        sideBar.setSize(200, 720);
        sideBar.setTouchable(Touchable.childrenOnly);
        sideBar.setBackground(uiSkin.getDrawable("sideBar"));

        buttons = new Table();
        buttons.add(startWaveBtn).size(190, 60).row();
        buttons.add(backToMapBtn).size(190, 60).row();
        buttons.add(buildBtn).size(190, 60).row();
        buttons.add(sellBtn).size(190, 60).row();
        buttons.add(upgradeTowerBtn).size(190, 60).row();
        buttons.add(gemBtn).size(190, 60).row();


        Image gold = new Image(this.gold);
        Image heart = new Image(this.lives);
        goldLives = new Table();
        goldLives.setBackground(uiSkin.getDrawable("statsBg"));
        goldLives.add(gold).size(50);
        goldLives.add(levelScreen.getGoldAmountLabel()).right().size(140, 50);
        goldLives.row();
        goldLives.add(heart).size(50);
        goldLives.add(levelScreen.getLivesLabel()).right().size(140, 50);

        sideBar.add(buttons).pad(5, 5, 0, 5).size(190, 360).row();
        sideBar.add(goldLives).pad(0, 5, 0, 5).size(190, 100).row();
        sideBar.align(Align.top);

        setListeners();
        deselectCell();
        setUpDialogs();
    }
    public void deselectCell() {
        sellBtn.setColor(1, 1, 1, 0.5f);
        buildBtn.setColor(1, 1, 1, 0.5f);
        upgradeTowerBtn.setColor(1, 1, 1, 0.5f);
        buildBtn.setDisabled(true);
        sellBtn.setDisabled(true);
        upgradeTowerBtn.setDisabled(true);
        sellBtn.setTouchable(Touchable.disabled);
        upgradeTowerBtn.setTouchable(Touchable.disabled);
        buildBtn.setTouchable(Touchable.disabled);
        if (rangeCircle != null)
            rangeCircle.remove();
        selectImg.remove();
        selectedCell = null;
        selected = false;
    }

    public boolean isSelect() {
        return selected;
    }

    public void setSelect(boolean selectedTower) {
        this.selected = selectedTower;
    }

    public void setSelectedCell(Tower selectedCell) {
        this.selectedCell = selectedCell;
    }

    public void addSide() {
        levelScreen.addActor(sideBar);
        sideBar.setZIndex(levelScreen.getActors().size + 1);
    }

    private void reAddSide() {
        sideBar.setZIndex(levelScreen.getActors().size + 1);
    }

    public void select(int x, int y) {
        if (levelScreen.isWave()) {
            if (((GroundCell) levelScreen.getField()[x][y]).isHaveTower() && levelScreen.getTowerMgr().getTowers()[x][y].hasGem()) {
                showRangeCircle();
                selected = true;
            }
        } else {
            if (levelScreen.getField()[x][y].getType() == FieldCell.TYPE_GROUND) {
                if (!((GroundCell) levelScreen.getField()[x][y]).isHaveTower()) {
                    enableBtnsEmpty();
                } else {
                    enableBtnsTower();
                    setSelectedCell(levelScreen.getTowerMgr().getTowers()[x][y]);
                }
                selected = true;
            }
        }
    }

    public void showRangeCircle() {
        selected = true;
        if (rangeCircle != null)
            rangeCircle.remove();
        Tower tower = levelScreen.getTowerMgr().getTowers()[levelScreen.getCurTwrX()][levelScreen.getCurTwrY()];
        if (tower != null && tower.hasGem()) {

            rangeTxtr = new Texture(Gdx.files.internal("Gems/rangeCircle.png"));
            rangeCircle = new Image(rangeTxtr);
            rangeCircle.setSize(tower.getRange() * 2, tower.getRange() * 2);
            rangeCircle.setPosition(tower.getPosX(), tower.getPosY(), Align.center);
            rangeCircle.setTouchable(Touchable.disabled);
            levelScreen.addActor(rangeCircle);
        }
        reAddSide();
    }

    public void enableBtnsTower() {
        selectImg.setPosition(levelScreen.getCurTwrX() * Config.CELL_W, levelScreen.getCurTwrY() * Config.CELL_H);
        levelScreen.addActor(selectImg);
        buildBtn.setColor(1, 1, 1, 0.5f);
        sellBtn.setColor(1, 1, 1, 1);
        upgradeTowerBtn.setColor(1, 1, 1, 1);
        buildBtn.setDisabled(true);
        sellBtn.setDisabled(false);
        upgradeTowerBtn.setDisabled(false);
        buildBtn.setTouchable(Touchable.disabled);
        sellBtn.setTouchable(Touchable.enabled);
        upgradeTowerBtn.setTouchable(Touchable.enabled);
        showRangeCircle();
    }

    public void enableBtnsEmpty() {
        selectImg.setPosition(levelScreen.getCurTwrX() * Config.CELL_W, levelScreen.getCurTwrY() * Config.CELL_H);
        levelScreen.addActor(selectImg);
        showRangeCircle();

        buildBtn.setColor(1, 1, 1, 1);
        sellBtn.setColor(1, 1, 1, 0.5f);
        upgradeTowerBtn.setColor(1, 1, 1, 0.5f);
        buildBtn.setDisabled(false);
        sellBtn.setDisabled(true);
        upgradeTowerBtn.setDisabled(true);

        buildBtn.setTouchable(Touchable.enabled);
        sellBtn.setTouchable(Touchable.disabled);
        upgradeTowerBtn.setTouchable(Touchable.disabled);
        reAddSide();

    }

    public void setListeners() {

        backToMapBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                CURRENT_DIALOG = DIALOG_QUIT;
                showDealDialog(0);
            }
        });

        startWaveBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                setMenuWave();
                selected = false;
                selectImg.remove();

                wave = new Wave();
                levelScreen.addWaveNum();
                wave.setWave(levelScreen);
                levelScreen.setWave(true);

                levelScreen.getEnemyTimer().scheduleTask(new Timer.Task() {

                                                             @Override
                                                             public void run() {
                                                                 AbstractEnemy enemy = wave.getEnemies().get(0);
                                                                 enemy.updateImage();
                                                                 levelScreen.getEnemies().add(enemy);
                                                                 levelScreen.addActor(wave.getEnemies().get(0).getImage());
                                                                 reAddSide();
                                                                 wave.getEnemies().remove(0);
                                                                 if (wave.getEnemies().size() == 0) levelScreen.allowFinish();
                                                             }
                                                         }, 0,
                        wave.getPeriod(),
                        wave.getEnemies().size() - 1);

            }
        });
        gemBtn.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().showGemScreen(selectedCell);
                levelScreen.pauseLevel();
            }
        });

        buildBtn.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                CURRENT_DIALOG = DIALOG_BUILD;
                showDealDialog(Config.TOWER_COST);
            }
        });

        sellBtn.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                CURRENT_DIALOG = DIALOG_SELL;
                showDealDialog(Config.TOWER_DESTROY_GET_DEF *
                        levelScreen.getTowerMgr().getTowers()[levelScreen.getCurTwrX()][levelScreen.getCurTwrY()].getRank());
            }
        });

        upgradeTowerBtn.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                CURRENT_DIALOG = DIALOG_UPGRADE;
                showDealDialog(Config.TOWER_UP_COST_DEF *
                        levelScreen.getTowerMgr().getTowers()[levelScreen.getCurTwrX()][levelScreen.getCurTwrY()].getRank());
            }
        });

    }

    public void setMenuBuild() {
        deselectCell();
        startWaveBtn.setDisabled(false);
        startWaveBtn.setColor(1, 1, 1, 1);
        startWaveBtn.setTouchable(Touchable.enabled);
    }

    public void setMenuWave() {
        deselectCell();
        startWaveBtn.setDisabled(true);
        startWaveBtn.setColor(1, 1, 1, 0.5f);
        startWaveBtn.setTouchable(Touchable.disabled);

    }

    public void setUpDialogs() {
        dealTimer = new Timer();
        winDialog = new Dialog("", uiSkin);
        winDialog.text("Уровень пройден!", uiSkin.optional("dialogText", Label.LabelStyle.class));
        TextButton winExit = new TextButton("Выйти на карту", uiSkin.optional("dialogBtn", TextButton.TextButtonStyle.class));
        winDialog.button(winExit);
        winExit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                new Timer().scheduleTask(new Timer.Task() {
                    @Override
                    public void run() {
                        ScreenManager.getInstance().showMap();
                    }
                }, 0.4f);
            }
        });


        loseDialog = new Dialog("", uiSkin);
        loseDialog.text("Уровень проигран!", uiSkin.optional("dialogText", Label.LabelStyle.class));

        TextButton loseExit = new TextButton("Выйти на карту", uiSkin.optional("dialogBtn", TextButton.TextButtonStyle.class));
        loseDialog.button(loseExit);
        loseExit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                new Timer().scheduleTask(new Timer.Task() {
                    @Override
                    public void run() {
                        ScreenManager.getInstance().showMap();
                    }
                }, 0.4f);
            }
        });

        TextButton loseRetry = new TextButton("Начать заново", uiSkin.optional("dialogBtn", TextButton.TextButtonStyle.class));
        loseDialog.button(loseRetry);
        loseRetry.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                new Timer().scheduleTask(new Timer.Task() {
                    @Override
                    public void run() {
                        levelScreen.reset();
                    }
                }, 0.4f);
            }
        });
        dealDialog = new Dialog("", uiSkin);
        dealDescription = new Label("", uiSkin.optional("dialogText", Label.LabelStyle.class));
        dealDescription.setSize(1200, 375);
        dealDescription.setAlignment(Align.center);
        dealDialog.text(dealDescription);

        TextButton.TextButtonStyle textButtonStyle = uiSkin.optional("dialogBtn", TextButton.TextButtonStyle.class);
        dealDialog.getButtonTable().pad(15, 112.5f, 15, 112.5f);
        yesDeal = new TextButton("Да", textButtonStyle);
        yesDeal.align(Align.center);
        yesDeal.setSize(375, 120);
        yesDeal.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                switch (CURRENT_DIALOG) {
                    case DIALOG_BUILD: {
                        dealTimer.scheduleTask(new Timer.Task() {
                            @Override
                            public void run() {
                                levelScreen.takeGold(Config.TOWER_COST);
                                levelScreen.getTowerMgr().buildTower(levelScreen.getCurTwrX(), levelScreen.getCurTwrY());
                                selectedCell = levelScreen.getTowerMgr().getTowers()[levelScreen.getCurTwrX()][levelScreen.getCurTwrY()];
                                enableBtnsTower();
                                levelScreen.unpauseLevel();
                            }
                        }, 0.4f);
                        break;
                    }
                    case DIALOG_SELL: {
                        final Tower tower = levelScreen.getTowerMgr().getTowers()[levelScreen.getCurTwrX()][levelScreen.getCurTwrY()];
                        dealTimer.scheduleTask(new Timer.Task() {
                            @Override
                            public void run() {
                                levelScreen.addGold(Config.TOWER_DESTROY_GET_DEF * tower.getRank());
                                levelScreen.getTowerMgr().removeTower(tower);
                                enableBtnsEmpty();
                                levelScreen.unpauseLevel();

                            }
                        }, 0.4f);

                        break;
                    }
                    case DIALOG_UPGRADE: {
                        final Tower tower = levelScreen.getTowerMgr().getTowers()[levelScreen.getCurTwrX()][levelScreen.getCurTwrY()];

                        dealTimer.scheduleTask(new Timer.Task() {
                            @Override
                            public void run() {
                                levelScreen.takeGold(Config.TOWER_UP_COST_DEF * tower.getRank());
                                levelScreen.getTowerMgr().upgradeTower(tower);
                                showRangeCircle();
                                levelScreen.unpauseLevel();
                            }
                        }, 0.4f);

                        break;
                    }

                    case DIALOG_QUIT: {
                        dealTimer.scheduleTask(new Timer.Task() {
                            @Override
                            public void run() {
                                for (int i = 0; i < levelScreen.getActors().size; i++) {
                                    levelScreen.getActors().get(i).setTouchable(Touchable.disabled);
                                }
                                ScreenManager.getInstance().showMap();
                            }
                        }, 0.4f);
                        break;
                    }
                }
                dealDialog.hide();
            }
        });

        TextButton noDeal = new TextButton("Нет", textButtonStyle);
        noDeal.align(Align.center);
        noDeal.setSize(375, 120);
        noDeal.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                dealTimer.scheduleTask(new Timer.Task() {
                    @Override
                    public void run() {
                        levelScreen.unpauseLevel();
                    }
                }, 0.4f);
            }
        });
        //dealDialog.getButtonTable().add(yesDeal);
        dealDialog.button(yesDeal);
        dealDialog.button(noDeal);
    }

    public void showDealDialog(int gold) {
        levelScreen.pauseLevel();
        switch (CURRENT_DIALOG) {
            case DIALOG_BUILD: {
                dealDescription.setText("Вы уверены, что хотите построить башню?" +
                        "\nЭто будет стоить " + gold + " золота.");
                break;
            }
            case DIALOG_SELL: {
                dealDescription.setText("Вы уверены, что хотите снести башню?" +
                        "\nЭто принесёт вам " + gold + " золота.");

                break;
            }
            case DIALOG_UPGRADE: {
                dealDescription.setText("Вы уверены, что хотите улучшить башню?" +
                        "\nЭто будет стоить " + gold + " золота.");

                break;
            }
            case DIALOG_QUIT: {
                dealDescription.setText("Вы уверены, что хотите выйти с уровня?" +
                        "\n Ваш прогресс будет утерян.");

                break;
            }
        }
        if (levelScreen.getGold() >= gold || CURRENT_DIALOG == DIALOG_SELL || CURRENT_DIALOG == DIALOG_QUIT) {
            yesDeal.setText("Да");
            yesDeal.setDisabled(false);
            yesDeal.setColor(1, 1, 1, 1);
            yesDeal.setTouchable(Touchable.enabled);
        } else {
            yesDeal.setText("Недостаточно золота!");
            yesDeal.setDisabled(true);
            yesDeal.setColor(0.5f, .5f, .5f, 1);
            yesDeal.setTouchable(Touchable.disabled);
        }
        dealDialog.show(levelScreen);
    }

    public void lose() {
        loseDialog.show(levelScreen);
        levelScreen.pauseLevel();
    }

    public void win() {
        winDialog.show(levelScreen);
        levelScreen.pauseLevel();
    }

    public void dispose() {
        uiSkin.dispose();
        select.dispose();
        if (rangeTxtr != null)
            rangeTxtr.dispose();
        gold.dispose();
        lives.dispose();
    }

    public void setWave(Wave wave) {
        this.wave = wave;
    }

    public Wave getWave() {
        return wave;
    }
}
