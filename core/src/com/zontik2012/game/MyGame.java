package com.zontik2012.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.zontik2012.game.Managers.ScreenManager;
import com.zontik2012.game.Screens.AbstractScreen;
import com.zontik2012.game.util.Assets;

public class MyGame extends Game {
    private Assets assets;


    @Override
    public void create() {
        assets=new Assets();
        ScreenManager.getInstance().initialize(this);
        ScreenManager.getInstance().startGame();
        //ScreenManager.getInstance().startTest();
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public AbstractScreen getScreen() {
        return (AbstractScreen) screen;
    }

    @Override
    public void setScreen(Screen screen) {
        super.setScreen(screen);
    }

    public Assets getAssets() {
        return assets;
    }
}
