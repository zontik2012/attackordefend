package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class Slow extends Buff {
    /**
     * Debuff which decreases enemy movespeed.
     *
     * @param amount   Percentage of speed decreased
     * @param duration Debuff duration
     */
    public Slow(float amount, float duration, Gem source) {
        super(TYPE_MOVESPEED, -amount, duration, source);
    }
}
