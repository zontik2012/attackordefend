package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class InstantHeal extends Buff {
    /**
     * Buff which heals once as soon as possible
     *
     * @param amount Amount of health restored
     */
    public InstantHeal(float amount, Gem source) {
        super(TYPE_HP_CUR, amount, 0, source);
    }
}
