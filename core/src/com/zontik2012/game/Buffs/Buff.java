package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class Buff {
    public static final int TYPE_DEFENSE = 0;
    public static final int TYPE_HP_MAX = 1;
    public static final int TYPE_HP_CUR = 2;
    public static final int TYPE_HP_REGEN = 3;
    public static final int TYPE_MOVESPEED = 4;
    public static final int TYPE_STUN = 5;
    public static final int TYPES_AMOUNT = 6;

    private int type;
    private float amount;
    private float duration, remains;
    private Gem source;

    public Buff(int type, float amount, float duration, Gem source) {
        this.type = type;
        this.amount = amount;
        this.duration = duration;
        this.source = source;
    }

    public void update(float delta) {
        duration -= delta;
    }

    public int getType() {
        return type;
    }

    public float getAmount() {
        return amount;
    }

    public float getDuration() {
        return duration;
    }

    public float getRemains() {
        return remains;
    }

    public Gem getSource() {
        return source;
    }
}
