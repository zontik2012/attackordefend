package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class BurstDamage extends Buff {
    /**
     * Instant damage dealt once as soon as posible.
     *
     * @param amount Damage dealt
     */
    public BurstDamage(float amount, Gem source) {
        super(TYPE_HP_CUR, -amount, 0, source);
    }
}
