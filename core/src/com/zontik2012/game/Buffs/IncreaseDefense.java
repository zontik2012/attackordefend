package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class IncreaseDefense extends Buff {
    /**
     * Buff which increases enemy defense, decreasing damage dealt from all sources of damage.
     *
     * @param amount   Amount of percents of defense added.
     * @param duration Buff duration
     */
    public IncreaseDefense(float amount, float duration, Gem source) {
        super(TYPE_DEFENSE, amount, duration, source);
    }
}
