package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class DamageOverTime extends Buff {
    /**
     * Debuff which deals damage over time
     *
     * @param amount   Amount of Damage dealt per Second
     * @param duration Debuff duration
     */
    public DamageOverTime(float amount, float duration, Gem source) {
        super(TYPE_HP_REGEN, -amount, duration, source);
    }
}
