package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class Haste extends Buff {
    /**
     * Buff which increases enemy movespeed.
     *
     * @param amount   Percentage of speed increased
     * @param duration Buff duration
     */
    public Haste(float amount, float duration, Gem source) {
        super(TYPE_MOVESPEED, amount, duration, source);
    }
}
