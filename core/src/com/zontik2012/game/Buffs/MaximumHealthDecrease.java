package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class MaximumHealthDecrease extends Buff {
    /**
     * Debuff decreases enemy maximum health
     *
     * @param amount   Amount of percentage of health removed
     * @param duration Debuff duration
     */
    public MaximumHealthDecrease(float amount, float duration, Gem source) {
        super(TYPE_HP_MAX, amount, duration, source);
    }
}
