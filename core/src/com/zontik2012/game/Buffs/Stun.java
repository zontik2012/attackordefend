package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class Stun extends Buff {
    /**
     * Debuff which prevents enemy from moving
     *
     * @param duration Stun duration
     */
    public Stun(float duration, Gem source) {
        super(TYPE_STUN, 0, duration, source);
    }
}
