package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class Regeneration extends Buff {
    /**
     * Buff increases enemy health regeneration
     *
     * @param amount   Amount of health regenerated per second
     * @param duration Buff duration
     */
    public Regeneration(float amount, float duration, Gem source) {
        super(TYPE_HP_REGEN, amount, duration, source);
    }
}
