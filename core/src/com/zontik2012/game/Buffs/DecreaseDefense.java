package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class DecreaseDefense extends Buff {
    /**
     * Buff which decreases enemy defense, increasing damage dealt from all sources of damage.
     *
     * @param amount   Amount of percents of defense removed.
     * @param duration Debuff duration
     */
    public DecreaseDefense(float amount, float duration, Gem source) {
        super(TYPE_DEFENSE, -amount, duration, source);
    }
}
