package com.zontik2012.game.Buffs;

import com.zontik2012.game.Gems.Gem;

/**
 * Created by zontik2012 on 08.12.2017.
 */

public class MaximumHealthIncrease extends Buff {
    /**
     * Buff increases enemy maximum health
     *
     * @param amount   Amount of percentage of health added
     * @param duration Buff duration
     */
    public MaximumHealthIncrease(float amount, float duration, Gem source) {
        super(TYPE_HP_MAX, amount, duration, source);
    }
}
