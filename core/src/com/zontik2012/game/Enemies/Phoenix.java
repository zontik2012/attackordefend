package com.zontik2012.game.Enemies;

import com.zontik2012.game.util.Assets;

public class Phoenix extends AbstractEnemy {
    public Phoenix() {
        super(Assets.phoenixAtlas);
        setScale(2f);
        setMaxHp(300);
        setSpeed(30);
    }
}
