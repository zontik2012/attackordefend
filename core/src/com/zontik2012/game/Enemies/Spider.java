package com.zontik2012.game.Enemies;

import com.zontik2012.game.util.Assets;

public class Spider extends AbstractEnemy {
    public Spider() {
        super(Assets.spiderAtlas);
        setScale(0.5f);
        setMaxHp(50);
        setSpeed(25);
    }
}
