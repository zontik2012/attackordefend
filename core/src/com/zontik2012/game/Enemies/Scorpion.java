package com.zontik2012.game.Enemies;

import com.zontik2012.game.util.Assets;

public class Scorpion extends AbstractEnemy {
    public Scorpion() {
        super(Assets.skorpionAtlas);
        setScale(0.5f);
        setMaxHp(50);
        setSpeed(25);
    }
}
