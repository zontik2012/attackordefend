package com.zontik2012.game.Enemies;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.zontik2012.game.Buffs.Buff;
import com.zontik2012.game.Bullets.SmartBullet;
import com.zontik2012.game.Field.FieldCell;
import com.zontik2012.game.Field.RoadCell;
import com.zontik2012.game.Screens.LevelScreen;
import com.zontik2012.game.Towers.Tower;
import com.zontik2012.game.util.Config;

import java.util.ArrayList;
import java.util.Random;

/**
 * Base enemy class. Contains all methods needed to create enemies and interact with them
 */
public abstract class AbstractEnemy {
    //image - Изображение врага;
    //animation - Анимация для картинки;
    //assetDescriptor - описание файла для assetManager
    private Image image;
    private Animation<Image> animation;
    private AssetDescriptor<TextureAtlas> assetDescriptor;
    //stateTime - время для анимации врага,scale-Размер картинки, 1.0 - размер клетки.
    private float stateTime;
    private float scale = 1;
    //Размеры изображения, чтобы в клетку влезал
    private int frameW;
    private int frameH;
    //Скорость, обычная и под баффами/дебаффами, скорость по осям, координаты начальной точки
    private int defaultSpeed, buffedSpeed, startX, startY;
    //Набор баффов/дебаффов, которым подвержен враг в текущий момент
    private ArrayList<Buff> buffs;
    //Оглушён ли враг
    private boolean stunned;
    //Текущее направление движения. 0-вверх,1-вправо,2-вниз,3-влево
    private int direction = -1;
    //Реально текущие координаты (центр картинки, ставим через Align.center)
    private float X, Y;
    //Максимальное, максимальное под баффами/дебаффами и текущее здоровье, защита(броня) обычная и под баффами/дебаффами
    private float defaultMaxHp, buffedMaxHp, curHP, defaultDefense, buffedDefense;
    //Башни, которые стреляют по данному врагу и самонаводящиеся снаряды, которые преследуют данного врага
    private ArrayList<Tower> focusingThisTowers;
    private ArrayList<SmartBullet> focusingThisSmartBullets;

    //Экран с уровнем, клетка в которой сейчас, поле боя
    private LevelScreen levelScreen;
    private FieldCell now;
    private FieldCell[][] fieldCells;

    //visitedCells - массив, отмечающий пройденные клетки;
    //dirFlag - для выхода из рекурсии при поиске пути;
    //ways - куда можно идти, индексы=направления;
    //reachedEnd - достиг ли конца пути
    private boolean visitedCell[][], dirFlag, ways[], reachedEnd;

    /**
     * @param assetDescriptor Asset Description taken from AssetManager.
     */
    AbstractEnemy(AssetDescriptor<TextureAtlas> assetDescriptor) {
        startX = Config.CELL_W / 2 + (new Random().nextInt(21) - 10);
        startY = Config.CELL_H / 2 + (new Random().nextInt(21) - 10);
        this.assetDescriptor = assetDescriptor;
        buffs = new ArrayList<Buff>();
        focusingThisSmartBullets = new ArrayList<SmartBullet>();
        focusingThisTowers = new ArrayList<Tower>();
    }

    public void move(float delta) {
        updateBuffs(delta);
        if (!stunned) {
            //Смена направления
            int fX = ((int) X) / Config.CELL_W, fY = ((int) Y) / Config.CELL_H;
            if (fX >= 0 && fX < 15 && fY >= 0 && fY < 10) {
                FieldCell fieldCell = fieldCells[fX][fY];
                if (now != fieldCell) {
                    now = fieldCell;
                    for (int i = 0; i < 4; i++) {
                        ways[i] = false;
                        int nx = fX + Config.DX[i], ny = fY + Config.DY[i];
                        if (nx >= 0 &&
                                nx < 15 &&
                                ny >= 0 &&
                                ny < 10 &&
                                fieldCells[nx][ny].getType() == FieldCell.TYPE_ROAD &&
                                !visitedCell[nx][ny]) {
                            boolean t_visited[][] = new boolean[15][10];
                            for (int j = 0; j < 15; j++) {
                                System.arraycopy(visitedCell[j], 0, t_visited[j], 0, 10);
                            }
                            t_visited[fX][fY] = true;
                            if (checkDir(nx, ny, t_visited)) {
                                ways[i] = true;
                                dirFlag = false;
                            }
                        }
                    }
                }
                if (checkTurn(X, Y) && !visitedCell[fX][fY]) {
                    visitedCell[fX][fY] = true;
                    if (((RoadCell) fieldCell).isEnd()) {
                        reachedEnd = true;
                    } else {
                        int c = 0, dir = 0;
                        for (int i = 0; i < 4; i++) {
                            if (ways[i]) c++;
                        }
                        c = new Random().nextInt(c);
                        for (int i = 0; i < 4; i++) {
                            if (ways[i]) {
                                if (c == 0) {
                                    dir = i;
                                    break;
                                }
                                c--;
                            }
                        }
                        direction = dir;
                    }
                }
            }

            //Движение
            X += buffedSpeed * Config.DX[direction] * delta;
            Y += buffedSpeed * Config.DY[direction] * delta;

            //Анимация
            stateTime += delta;
            updateImage();
        }
    }

    private boolean checkTurn(float intX, float intY) {
        switch (direction) {
            case RoadCell.DIRECTION_UP: {
                return intY % Config.CELL_H >= startY;
            }
            case RoadCell.DIRECTION_RIGHT: {
                return intX % Config.CELL_W >= startX;
            }
            case RoadCell.DIRECTION_DOWN: {
                return intY % Config.CELL_H <= startY;
            }
            case RoadCell.DIRECTION_LEFT: {
                return intX % Config.CELL_W <= startX;
            }
        }
        return false;
    }

    /**
     * @param frameLength 1 equals to 1 sec
     * @param scale       1 equals to normal picture size
     */
    private void setImageAtlas(Animation.PlayMode playMode, float frameLength, float scale) {
        TextureAtlas atlas = levelScreen.getAssets().manager.get(assetDescriptor);
        Array<Image> frames = new Array<Image>();
        for (int i = 0; i < atlas.getRegions().size; i++)
            frames.add(new Image(atlas.getRegions().get(i)));
        animation = new Animation<Image>(frameLength, frames);
        animation.setPlayMode(playMode);

        image = new Image(animation.getKeyFrame(0).getDrawable());

        if (image.getWidth() > image.getHeight()) {

            this.frameW = Config.CELL_W;
            this.frameH = (int) (image.getHeight() * (Config.CELL_W / image.getWidth()));

        } else if (image.getWidth() < image.getHeight()) {

            this.frameH = Config.CELL_H;
            this.frameW = (int) (image.getWidth() * (Config.CELL_H / image.getHeight()));

        } else {

            this.frameW = Config.CELL_W;
            this.frameH = Config.CELL_H;

        }

        image.setSize(frameW, frameH);
        image.setScale(scale);
        image.setOrigin(image.getWidth() / 2, image.getHeight() / 2);
        stateTime = new Random().nextInt(frames.size) * animation.getFrameDuration();
    }

    public void updateImage() {
        image.setDrawable(animation.getKeyFrame(stateTime).getDrawable());
        switch (direction) {
            case 0:
                image.setRotation(0);
                break;
            case 1:
                image.setRotation(-90);
                break;
            case 2:
                image.setRotation(180);
                break;
            case 3:
                image.setRotation(90);
                break;
        }
        image.setPosition(X, Y, Align.center);
    }

    private void updateBuffs(float delta) {
        stunned = false;
        buffedDefense = defaultDefense;
        buffedMaxHp = defaultMaxHp;
        buffedSpeed = defaultSpeed;
        for (int i = 0; i < Buff.TYPES_AMOUNT; i++) {
            for (int j = 0; j < buffs.size(); j++) {
                Buff buff = buffs.get(j);
                if (buff.getType() == i) {
                    switch (buff.getType()) {
                        case Buff.TYPE_DEFENSE: {
                            buffedDefense += defaultDefense * buff.getAmount();
                            break;
                        }
                        case Buff.TYPE_HP_MAX: {
                            buffedMaxHp += defaultMaxHp * buff.getAmount();
                            curHP = (buffedMaxHp / defaultMaxHp) * curHP;
                            break;
                        }
                        case Buff.TYPE_HP_CUR: {
                            curHP += buff.getAmount();
                            break;
                        }
                        case Buff.TYPE_HP_REGEN: {
                            curHP += buff.getAmount() * delta;
                            break;
                        }
                        case Buff.TYPE_MOVESPEED: {
                            buffedSpeed += defaultSpeed * buff.getAmount();
                            break;
                        }
                        case Buff.TYPE_STUN: {
                            stunned = true;
                            break;
                        }
                    }
                    buff.update(delta);
                    if (buff.getDuration() <= 0)
                        buffs.remove(j--);
                }
            }
        }
    }

    public void addFocusingTower(Tower tower) {
        focusingThisTowers.add(tower);
    }

    public void stopFocusingWhenDie() {
        for (int i = 0; i < focusingThisTowers.size(); i++) {
            focusingThisTowers.get(i).getTargets().remove(this);
        }
        for (int i = 0; i < focusingThisSmartBullets.size(); i++) {
            focusingThisSmartBullets.get(i).targetDied();
        }
        this.remove();
    }

    public void remove() {
        image.remove();
    }

    public void damage(int damage) {
        curHP -= damage;
    }

    public void setLevelScreen(LevelScreen levelScreen) {
        ways = new boolean[4];
        visitedCell = new boolean[15][10];
        this.levelScreen = levelScreen;
        fieldCells = levelScreen.getField();
        int sX = levelScreen.getFieldCreator().getStartX(), sY = levelScreen.getFieldCreator().getStartY();
        X = sX * Config.CELL_W + startX;
        Y = sY * Config.CELL_H + startY;
        if (sX == 0) {
            direction = RoadCell.DIRECTION_RIGHT;
            X -= Config.CELL_W;

        } else if (sX == 14) {
            direction = RoadCell.DIRECTION_LEFT;
            X += Config.CELL_W;

        } else if (sY == 0) {
            direction = RoadCell.DIRECTION_UP;
            Y -= Config.CELL_H;
        } else {
            direction = RoadCell.DIRECTION_DOWN;
            Y += Config.CELL_H;

        }
        setImageAtlas(Animation.PlayMode.LOOP_PINGPONG, 0.15f, scale);
    }

    public void addFocusingSmartBullet(SmartBullet smartBullet) {
        focusingThisSmartBullets.add(smartBullet);
    }

    private boolean checkDir(int x, int y, boolean visited[][]) {
        visited[x][y] = true;
        if (((RoadCell) fieldCells[x][y]).isEnd()) {
            dirFlag = true;
        }
        for (int i = 0; i < 4 && !dirFlag; i++) {
            int nx = x + Config.DX[i], ny = y + Config.DY[i];
            if (!dirFlag) {
                if (nx >= 0 &&
                        nx < 15 &&
                        ny >= 0 &&
                        ny < 10 &&
                        fieldCells[nx][ny].getType() == FieldCell.TYPE_ROAD &&
                        !visited[nx][ny])
                    checkDir(nx, ny, visited);
            }
        }
        visited[x][y] = false;
        return dirFlag;
    }

    void setScale(float scale) {
        this.scale = scale;
    }

    void setSpeed(int speed) {
        this.defaultSpeed = speed;
        buffedSpeed = speed;
    }

    public float getX() {
        return X;
    }

    public float getY() {
        return Y;
    }

    public float getWidth() {
        return frameW * scale;
    }

    public float getHeight() {
        return frameH * scale;
    }

    public float getCurHP() {
        return curHP;
    }

    public boolean isReachedEnd() {
        return reachedEnd;
    }

    void setMaxHp(int maxHp) {
        this.defaultMaxHp = maxHp;
        buffedMaxHp = maxHp;
    }

    public Image getImage() {
        if (image == null) updateImage();
        return image;
    }

}